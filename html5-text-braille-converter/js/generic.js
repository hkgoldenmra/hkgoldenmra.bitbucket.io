function textToBrailles(text) {
	var brailles = [];
	for (var i = 0; i < text.length; i++) {
		var char = text.substring(i, i + 1);
		var data = textBrailleMap[char];
		brailles.push((data != undefined) ? data : char);
	}
	return brailles;
}

function braillesToBrailleLines(brailles, maxBraillesPerLine) {
	var brailleLines = [];
	for (var i = 0; i < brailles.length; i++) {
		if (i == 0) {
			brailleLines.push([brailles[i]]);
		} else if (brailles[i] == "\n") {
			brailleLines.push([]);
		} else {
			if (brailleLines[brailleLines.length - 1].join("").length + brailles[i].length > maxBraillesPerLine) {
				brailleLines.push([brailles[i]]);
			} else {
				brailleLines[brailleLines.length - 1].push(brailles[i]);
			}
		}
	}
	return brailleLines;
}

function brailleToBrailles(braille) {
	var brailles = [];
	for (var i = 0; i < braille.length; ) {
		for (var j = 3; j > 0; j--) {
			var char = braille.substring(i, i + j);
			if (brailleTextMap[char] != undefined) {
				brailles.push(char);
				i += j;
				break;
			} else if (j == 1) {
				brailles.push(char);
				i++;
			}
		}
	}
	return brailles;
}

function brailleToText(braille) {
	var texts = [];
	for (var i = 0; i < braille.length; ) {
		for (var j = 3; j > 0; j--) {
			var char = braille.substring(i, i + j);
			var data = brailleTextMap[char];
			if (data != undefined) {
				texts.push(data[0]);
				i += j;
				break;
			} else if (j == 1) {
				texts.push(char);
				i++;
			}
		}
	}
	return texts;
}

function highlightSimulation(parentElement, array, start, end, altArray) {
	while (parentElement.hasChildNodes()) {
		parentElement.removeChild(parentElement.firstChild);
	}
	for (var i = 0; i < start; i++) {
		var element = document.createElement("span");
		element.textContent = array[i];
		if (altArray != undefined) {
			element.setAttribute("aria-label", altArray[i]);
			element.setAttribute("title", altArray[i]);
		}
		parentElement.appendChild(element);
	}
	for (var i = start; i < end; i++) {
		var element = document.createElement("span");
		element.className = "highlighted";
		element.textContent = array[i];
		if (altArray != undefined) {
			element.setAttribute("aria-label", altArray[i]);
			element.setAttribute("title", altArray[i]);
		}
		parentElement.appendChild(element);
	}
	for (var i = end; i < array.length; i++) {
		var element = document.createElement("span");
		element.textContent = array[i];
		if (altArray != undefined) {
			element.setAttribute("aria-label", altArray[i]);
			element.setAttribute("title", altArray[i]);
		}
		parentElement.appendChild(element);
	}
}

function formattedPrint(text, value) {
	var brailles = textToBrailles(text);
	if (value == 0) {
		return brailles.join("");
	} else if (value < 10) {
		return brailles.join("\n");
	} else {
		var brailleLines = braillesToBrailleLines(brailles, value);
		var brailleString = [];
		for (var i = 0; i < brailleLines.length; i++) {
			brailleString.push(brailleLines[i].join(""));
		}
		return brailleString.join("\n");
	}
}