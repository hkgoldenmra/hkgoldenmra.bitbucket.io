function GenericColor(element, value) {
	var colors = [
		"#FF0000", "#FF0800", "#FF1000", "#FF1800", "#FF2000", "#FF2800",
		"#FF3000", "#FF3800", "#FF4000", "#FF4800", "#FF5000", "#FF5800",
		"#FF6000", "#FF6800", "#FF7000", "#FF7800", "#FF8000", "#FF8800",
		"#FF9000", "#FF9800", "#FFA000", "#FFA800", "#FFB000", "#FFB800",
		"#FFC000", "#FFC800", "#FFD000", "#FFD800", "#FFE000", "#FFE800",
		"#E8FF00", "#E0FF00", "#D8FF00", "#D0FF00", "#C8FF00", "#C0FF00",
		"#B8FF00", "#B0FF00", "#A8FF00", "#A0FF00", "#98FF00", "#90FF00",
		"#88FF00", "#80FF00", "#78FF00", "#70FF00", "#68FF00", "#60FF00",
		"#58FF00", "#50FF00", "#48FF00", "#40FF00", "#38FF00", "#30FF00",
		"#28FF00", "#20FF00", "#18FF00", "#10FF00", "#08FF00", "#00FF00"
	];
	value = (value % 60 + 60) % 60;
	element.setAttribute("stroke", colors[value]);
}