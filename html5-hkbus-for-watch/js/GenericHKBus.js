function GenericHKBus() {
	// private
	// attributes
	var nameRegex = / ?[,(/／-].*/g;
	var routes = [];
	var stops = [];
	var etas = [];
	// methods
	var getKMBStop = function (stop_id) {
		var stop = {};
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("readystatechange", function (event) {
			if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
				var json = JSON.parse(this.responseText);
				stop = json.data;
			}
		});
		xhr.open("GET", "https://data.etabus.gov.hk/v1/transport/kmb/stop/" + stop_id, false);
		xhr.send();
		return stop;
	};
	var getCTBStop = function (stop_id) {
		var stop = {};
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("readystatechange", function (event) {
			if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
				var json = JSON.parse(this.responseText);
				stop = json.data;
			}
		});
		xhr.open("GET", "https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/stop/" + stop_id, false);
		xhr.send();
		return stop;
	};
	var getNWFBStop = function (stop_id) {
		var stop = {};
		var xhr = new XMLHttpRequest();
		xhr.addEventListener("readystatechange", function (event) {
			if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
				var json = JSON.parse(this.responseText);
				stop = json.data;
			}
		});
		xhr.open("GET", "https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/stop/" + stop_id, false);
		xhr.send();
		return stop;
	};
	//
	var padZero = function (value, length) {
		value += "";
		while (value.length < length) {
			value = "0" + value;
		}
		return value;
	};
	// public
	// methods
	this.getRoutes = function (filter, reload) {
		if (reload) {
			routes = [];
			var links = [
				"https://data.etabus.gov.hk/v1/transport/kmb/route",
				"https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/route/ctb",
				"https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/route/nwfb"
			];
			for (var i in links) {
				var xhr = new XMLHttpRequest();
				xhr.addEventListener("readystatechange", function (eventReadyStateChange) {
					if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
						var json = JSON.parse(this.responseText);
						routes = routes.concat(json.data);
					}
				});
				xhr.open("GET", links[i], false);
				xhr.send();
			}
		}
		if (filter == "") {
			return routes;
		} else {
			filter = filter.toUpperCase();
			var filterRoutes = [];
			for (var i in routes) {
				if (
					routes[i].route.toUpperCase().includes(filter) ||
					(
						routes[i].co == null &&
						(
							routes[i].dest_en.toUpperCase().includes(filter)
							|| routes[i].dest_tc.toUpperCase().includes(filter)
							|| routes[i].dest_sc.toUpperCase().includes(filter)
							)
						) ||
					(
						routes[i].co != null &&
						(
							routes[i].dest_en.toUpperCase().includes(filter)
							|| routes[i].dest_tc.toUpperCase().includes(filter)
							|| routes[i].dest_sc.toUpperCase().includes(filter)
							|| routes[i].orig_en.toUpperCase().includes(filter)
							|| routes[i].orig_tc.toUpperCase().includes(filter)
							|| routes[i].orig_sc.toUpperCase().includes(filter)
							)
						)
					) {
					filterRoutes.push(routes[i]);
				}
			}
			return filterRoutes;
		}
	};
	this.getStops = function (co, route, direction, service_type, filter, reload) {
		if (reload) {
			stops = [];
			direction = ((direction.toUpperCase() == "I") ? "in" : "out") + "bound";
			var links = {
				"KMB": ["https://data.etabus.gov.hk/v1/transport/kmb/route-stop", route, direction, service_type],
				"CTB": ["https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/route-stop/ctb", route, direction],
				"NWFB": ["https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/route-stop/nwfb", route, direction]
			};
			if (links[co] != null) {
				var xhr = new XMLHttpRequest();
				xhr.addEventListener("readystatechange", function (event) {
					if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
						var json = JSON.parse(this.responseText);
						stops = stops.concat(json.data);
					}
				});
				xhr.open("GET", links[co].join("/"), false);
				xhr.send();
			}
		}
		if (filter == "") {
			return stops;
		} else {
			filter = filter.toUpperCase();
			var filterStops = [];
			for (var i in stops) {
				if (false
					|| stops[i].seq.toUpperCase().includes(filter)
					|| stops[i].name_en.toUpperCase().includes(filter)
					|| stops[i].name_tc.toUpperCase().includes(filter)
					|| stops[i].name_sc.toUpperCase().includes(filter)) {
					filterStops.push(stops[i]);
				}
			}
			return filterStops;
		}
	};
	this.getETAs = function (co, stop_id, route, direction, service_type, reload) {
		if (reload) {
			etas = [];
			var links = {
				"KMB": ["https://data.etabus.gov.hk/v1/transport/kmb/eta", stop_id, route, service_type],
				"CTB": ["https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/eta/ctb", stop_id, route],
				"NWFB": ["https://rt.data.gov.hk/v1.1/transport/citybus-nwfb/eta/nwfb", stop_id, route]
			};
			if (links[co] != null) {
				var xhr = new XMLHttpRequest();
				xhr.addEventListener("readystatechange", function (event) {
					if (this.readyState == XMLHttpRequest.DONE && this.status == 200) {
						var json = JSON.parse(this.responseText);
						etas = etas.concat(json.data);
					}
				});
				xhr.open("GET", links[co].join("/"), false);
				xhr.send();
			}
		}
		return etas;
	};
	// element methods
	this.buildRoutes = function (routeSelect, routeFilter, reload) {
		var routes = this.getRoutes(routeFilter.value, reload);
		while (routeSelect.options.length > 0) {
			routeSelect.remove(routeSelect.options[0]);
		}
		var option = document.createElement("option");
		option.innerHTML = "選擇路線";
		routeSelect.appendChild(option);
		for (var i in routes) {
			var co_name = "九巴";
			if (routes[i].co == "CTB") {
				co_name = "城巴";
			} else if (routes[i].co == "NWFB") {
				co_name = "新巴";
			}
			var service_name = "";
			if (routes[i].service_type > 1) {
				service_name = "特別班次";
			}
			if (routes[i].bound == null) {
				var option = document.createElement("option");
				option.innerHTML = [co_name, routes[i].route.split("").join(" "), routes[i].orig_tc.replace(nameRegex, ""), service_name].join(";");
				option.value = [routes[i].co, routes[i].route, "I", routes[i].service_type].join(",");
				routeSelect.appendChild(option);
				var option = document.createElement("option");
				option.innerHTML = [co_name, routes[i].route.split("").join(" "), routes[i].dest_tc.replace(nameRegex, ""), service_name].join(";");
				option.value = [routes[i].co, routes[i].route, "O", routes[i].service_type].join(",");
				routeSelect.appendChild(option);
			} else {
				var option = document.createElement("option");
				option.innerHTML = [co_name, routes[i].route.split("").join(" "), routes[i].dest_tc.replace(nameRegex, ""), service_name].join(";");
				option.value = ["KMB", routes[i].route, routes[i].bound, routes[i].service_type].join(",");
				routeSelect.appendChild(option);
			}
		}
		var option = document.createElement("option");
		option.innerHTML = "資料結束";
		routeSelect.appendChild(option);
	};
	this.buildStops = function (routeSelect, stopSelect, stopFilter, reload) {
		var values = routeSelect.value.split(",");
		var stops = this.getStops(values[0], values[1], values[2], values[3], stopFilter.value, reload);
		while (stopSelect.options.length > 0) {
			stopSelect.remove(stopSelect.options[0]);
		}
		var option = document.createElement("option");
		option.innerHTML = "選擇車站";
		stopSelect.appendChild(option);
		for (var i in stops) {
			var stop = null;
			if (values[0] == "KMB") {
				stop = getKMBStop(stops[i].stop);
			} else if (values[0] == "CTB") {
				stop = getCTBStop(stops[i].stop);
			} else if (values[0] == "NWFB") {
				stop = getNWFBStop(stops[i].stop);
			}
			var option = document.createElement("option");
			option.innerHTML = [["第", stops[i].seq, "站"].join(""), stop.name_tc.replace(nameRegex, "")].join(":");
			option.value = [values[0], stops[i].stop].join(",");
			stopSelect.appendChild(option);
		}
		var option = document.createElement("option");
		option.innerHTML = "資料結束";
		stopSelect.appendChild(option);
	};
	this.buildETAs = function (routeSelect, stopSelect, etaContainer, reload) {
		var routeValues = routeSelect.value.split(",");
		var stopValues = stopSelect.value.split(",");
		var etas = this.getETAs(stopValues[0], stopValues[1], routeValues[1], routeValues[2], routeValues[3], reload);
		var etaTexts = etaContainer.getElementsByTagName("text");
		var index = 0;
		var now = new Date();
		for (var i in etas) {
			if (
				etas[i].eta != null &&
				etas[i].eta != "" &&
				etas[i].co == stopValues[0] &&
				(
					(
						etas[i].co == "KMB" &&
						etas[i].route == routeValues[1] &&
						etas[i].dir == routeValues[2] &&
						etas[i].service_type == routeValues[3]
						) ||
					(
						etas[i].co != "KMB" &&
						etas[i].route == routeValues[1] &&
						etas[i].dir == routeValues[2]
						)
					)
				) {
				var text = ["第", etas[i].eta_seq, "班"];
				var seconds = parseInt((new Date(etas[i].eta) - now) / 1000);
				if (seconds < 0) {
					seconds *= -1;
					text.push("超時");
					etaTexts[index].setAttribute("fill", "#FF0000");
				} else {
					text.push("還有");
					etaTexts[index].setAttribute("fill", "#00FF00");
				}
				text = text.join("");
				var hours = parseInt(seconds / 3600);
				seconds %= 3600;
				var minutes = parseInt(seconds / 60);
				seconds %= 60;
				text = [text, [padZero(hours, 2), "時", padZero(minutes, 2), "分", padZero(seconds, 2), "秒"].join("")].join(":");
				etaTexts[index].innerHTML = text;
				index++;
			}
		}
		while (index < etaTexts.length) {
			etaTexts[index].innerHTML = "";
			index++;
		}
	};
}