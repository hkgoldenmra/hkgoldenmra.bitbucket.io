function GenericArc(pathElement, centerX, centerY, radius, startDegrees, endDegrees) {
	var polarToCartesian = function (cx, cy, r, d) {
		var d = (d - 90) * Math.PI / 180;
		return {
			"x": cx + (r * Math.cos(d)),
			"y": cy + (r * Math.sin(d))
		};
	};
	var describeArc = function (x, y, r, sa, ea) {
		if (ea - sa >= 360) {
			ea = 359.999 - sa;
		}
		var s = polarToCartesian(x, y, r, ea);
		var e = polarToCartesian(x, y, r, sa);
		var f = ((ea - sa) > 180) ? 1 : 0;
		return [
			"M", s.x, s.y,
			"A", r, r, 0, f, 0, e.x, e.y
		].join(" ");
	};
	pathElement.setAttribute("d", describeArc(centerX, centerY, radius, startDegrees, endDegrees));
}