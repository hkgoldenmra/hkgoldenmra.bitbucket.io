// javascript.js

function WebBluetoothAPI(json) {
	this.connect = function() {
		navigator.bluetooth.requestDevice({
			"filters": [
				{
					"services": [
						json["service"]["uuid"]
					]
				}
			]
		}).then(function(bluetoothDevice) {
			console.log(bluetoothDevice);
			return bluetoothDevice.gatt.connect();
		}).then(function(bluetoothRemoteGATTServer) {
			console.log(bluetoothRemoteGATTServer);
			return bluetoothRemoteGATTServer.getPrimaryService(json["service"]["uuid"]);
		}).then(function(bluetoothRemoteGATTService) {
			console.log(bluetoothRemoteGATTService);
			return bluetoothRemoteGATTService.getCharacteristics();
		}).then(function(bluetoothRemoteGATTCharacteristics) {
			console.log(bluetoothRemoteGATTCharacteristics);
		});
	}
}