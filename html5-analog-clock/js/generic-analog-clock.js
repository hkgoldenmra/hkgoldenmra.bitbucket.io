function GenericAnalogClock(template){

	var canUpdate = false;

	var _updateHands = function(){
		if (canUpdate){
			var date = new Date();
			var hours = date.getHours() % 12;
			var minutes = date.getMinutes();
			var seconds = date.getSeconds();
			var milliseconds = date.getMilliseconds();
			var millisecondValue = milliseconds / 1000;
			var secondValue = (seconds + millisecondValue) / 60;
			var minuteValue = (minutes + secondValue) / 60;
			var hourValue = (hours + minuteValue) / 12;
			var millisecondsDegrees = 360 * millisecondValue;
			var secondsDegrees = 360 * secondValue;
			var minutesDegrees = 360 * minuteValue;
			var hoursDegrees = 360 * hourValue;
			_rotateHand(getFirstElementByClassName("millisecond-hand"), millisecondsDegrees);
			_rotateHand(getFirstElementByClassName("second-hand"), secondsDegrees);
			_rotateHand(getFirstElementByClassName("minute-hand"), minutesDegrees);
			_rotateHand(getFirstElementByClassName("hour-hand"), hoursDegrees);
			if (!!window.requestAnimationFrame){
				window.requestAnimationFrame(function(){
					_updateHands();
				});
			} else if (!!window.mozRequestAnimationFrame){
				window.mozRequestAnimationFrame(function(){
					_updateHands();
				});
			} else if (!!window.webkitRequestAnimationFrame){
				window.webkitRequestAnimationFrame(function(){
					_updateHands();
				});
			}
		}
	};

	var getFirstElementByClassName = function(className){
		var elements = template.getElementsByClassName(className);
		if (elements.length > 0){
			return elements[0];
		} else {
			return null;
		}
	};

	var _rotateHand = function(hand, degrees){
		var bBox = hand.getBBox();
		var cx = bBox.x + bBox.width / 2;
		var cy = bBox.y + bBox.height;
		hand.setAttribute('transform', "rotate(" + degrees + "," + cx + "," + cy + ")");
	};

	this.start = function(){
		if (!canUpdate){
			canUpdate = true;
			_updateHands();
		}
	};

	this.stop = function(){
		canUpdate = false;
	};
}
