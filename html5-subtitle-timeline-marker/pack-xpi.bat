@echo off

set exe_path=
set dist_dir=dist
set manifest_file=manifest.json

mklink /h "%manifest_file%" "firefox-manifest.json"

if not exist "%dist_dir%" (
	mkdir "%dist_dir%"
)
"%exe_path%\7z" a -tzip "%dist_dir%/html5-subtitlet-timeline-marker.xpi" "css" "js" "logo" "index.html" "%manifest_file%"

erase "%manifest_file%"
