@echo off

set exe_path=
set manifest_file=manifest.json

mklink /h "%manifest_file%" "firefox-manifest.json"

"%exe_path%\7z" a -tzip "html5-subtitle-timeline-marker.xpi.zip" "css" "js" "logo" "index.html" "%manifest_file%"

erase "%manifest_file%"
