#!/bin/bash

dist_dir="dist"
manifest_file="manifest.json"

ln "firefox-manifest.json" "${manifest_file}"

if ! [ -d "${dist_dir}" ]; then
	mkdir "${dist_dir}"
fi
7z a -tzip "${dist_dir}/html5-subtitle-timeline-marker.xpi" "css" "js" "logo" "index.html" "${manifest_file}"

rm -R "${manifest_file}"
