rem @echo off

set exe_path=
set temp_dir=temp
set dist_dir=dist
set temp_file=temp.txt

rmdir /q /s "%temp_dir%"
erase "%temp_dir%"
mkdir "%temp_dir%"

mklink /h "%temp_dir%\manifest.json" "chrome-manifest.json"
mklink /h "%temp_dir%\index.html" "index.html"
mklink /j "%temp_dir%\css" "css"
mklink /j "%temp_dir%\js" "js"
mklink /j "%temp_dir%\logo" "logo"

cd >"%temp_file%"
set /p dir=<"%temp_file%"
erase "%temp_file%"
"%exe_path%\chrome" --pack-extension="%dir%\%temp_dir%"

if not exist "%dist_dir%" (
	mkdir "%dist_dir%"
)
move "%temp_dir%.crx" "%dist_dir%/html5-subtitle-timeline-marker.crx"
move "%temp_dir%.pem" "%dist_dir%/html5-subtitle-timeline-marker.pem"

rmdir /q /s "%temp_dir%"
erase "%temp_dir%*"
