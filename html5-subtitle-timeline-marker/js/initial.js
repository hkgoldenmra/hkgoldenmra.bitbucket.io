window.addEventListener("load", function (loadEvent) {
	document.getElementById("select-source").addEventListener("dblclick", function (dblclickEvent) {
		if (this.selectedIndex >= 0) {
			var option = this.options[this.selectedIndex];
			if (option.hasAttribute("data-start")) {
				var media = document.getElementById("video-source");
				media.pause();
				media.currentTime = option.getAttribute("data-start") / 100;
			}
		}
	});
	document.getElementById("button-mark").addEventListener("click", function (clickEvent) {
		mark();
	});
	document.getElementById("button-load-media").addEventListener("click", function (clickEvent) {
		document.getElementById("file-media").click();
	});
	document.getElementById("button-load-text").addEventListener("click", function (clickEvent) {
		document.getElementById("file-text").click();
	});
	document.getElementById("button-export-txt").addEventListener("click", function (clickEvent) {
		exportTXT();
	});
	document.getElementById("button-export-srt").addEventListener("click", function (clickEvent) {
		exportSRT();
	});
	document.getElementById("button-export-ass").addEventListener("click", function (clickEvent) {
		exportASS();
	});
	document.getElementById("file-media").addEventListener("change", function (changeEvent) {
		importMediaFile(this);
	});
	document.getElementById("file-text").addEventListener("change", function (changeEvent) {
		importTextFile(this);
	});
	showSubtitle();
});

String.prototype.replaceAll = function (find, replace) {
	var index;
	var string = this;
	while ((index = string.indexOf(find)) >= 0) {
		var head = string.substr(0, index);
		var tail = string.substr(index + find.length);
		string = head + replace + tail;
	}
	return string;
}

function standarizeEOL(string) {
	return string.replaceAll("\r", "\n").replaceAll("\n\n", "\n");
}

function escapeXMLEntity(string) {
	return string.replaceAll("&", "&amp;").replaceAll("\"", "&quot;").replaceAll("<", "&lt;").replaceAll(">", "&gt;");
}

function millisecondsToTime(milliseconds) {
	var hours = Math.floor(milliseconds / 360000);
	milliseconds %= 360000;
	var minutes = Math.floor(milliseconds / 6000);
	milliseconds %= 6000;
	var seconds = Math.floor(milliseconds / 100);
	milliseconds %= 100;
	return padZero(hours, 2) +
		":" + padZero(minutes, 2) +
		":" + padZero(seconds, 2) +
		"." + padZero(milliseconds, 2);
}

function timeToMilliseconds(time) {
	var fields = time.split("\.", 3);
	var milliseconds = -fields[1];
	if (isNaN(milliseconds)) {
		return -1;
	} else {
		milliseconds = -(fields[1] + "00").substring(0, 2);
		fields = fields[0].split(":");
		milliseconds -= fields[2] * 100;
		milliseconds -= fields[1] * 6000;
		milliseconds -= fields[0] * 360000;
		return -milliseconds;
	}
}

function padZero(string, length) {
	string += "";
	while (string.length < length) {
		string = "0" + string;
	}
	return string;
}

function createObjectGUID(object) {
	if (window.createObjcectURL != undefined) {
		object = window.createOjcectURL(object);
	} else if (window.URL != undefined) {
		object = window.URL.createObjectURL(object);
	} else if (window.webkitURL != undefined) {
		object = window.webkitURL.createObjectURL(object);
	} else {
		object = null;
	}
	return object;
}

function importMediaFile(input) {
	var media = document.getElementById("video-source");
	var guid = createObjectGUID(input.files[0]);
	if (guid != null) {
		media.src = guid;
		if (media instanceof HTMLVideoElement) {
			media.addEventListener("resize", function (event) {
				if (media.readyState != HTMLMediaElement.HAVE_NOTHING) {
					media.height = media.videoHeight / (media.videoWidth / media.width);
				}
			});
		}
		if (media.parentNode instanceof HTMLAnchorElement) {
			media.parentNode.href = guid;
		}
	}
}

function importTextFile(input) {
	var list = document.getElementById("select-source");
	var start = document.getElementById("radio-marks");
	var fileReader = new FileReader();
	fileReader.readAsText(input.files[0]);
	fileReader.addEventListener("load", function (loadEvent) {
		while (list.options.length > 0) {
			list.remove(0);
		}
		var contents = standarizeEOL(this.result.trim()).split("\n");
		for (var i in contents) {
			var content = contents[i].trim().split("\,", 3);
			var option = document.createElement("option");
			if (content.length == 1) {
				option.value = content[0];
			} else if (content.length == 2) {
				option.value = content[1];
				var data;
				if ((data = timeToMilliseconds(content[0])) >= 0) {
					option.setAttribute("data-start", data);
				}
			} else {
				option.value = content[2];
				var data;
				if ((data = timeToMilliseconds(content[0])) >= 0) {
					option.setAttribute("data-start", data);
				}
				if ((data = timeToMilliseconds(content[1])) >= 0) {
					option.setAttribute("data-end", data);
				}
			}
			option.title = option.value;
			option.text = "," + option.value;
			if (option.hasAttribute("data-end")) {
				option.text = millisecondsToTime(option.getAttribute("data-end")) + option.text;
			}
			option.text = "," + option.text;
			if (option.hasAttribute("data-start")) {
				option.text = millisecondsToTime(option.getAttribute("data-start")) + option.text;
			}
			list.add(option);
		}
		list.selectedIndex = 0;
		start.checked = true;
	});
}

function mark() {
	var videoSource = document.getElementById("video-source");
	var selectSource = document.getElementById("select-source");
	var radioMarks = document.getElementById("radio-marks");
	var radioMarke = document.getElementById("radio-marke");
	var index = selectSource.selectedIndex;
	if (videoSource.error != null) {
		window.alert("Please import a video.");
	} else if (index < 0) {
		window.alert("Please select a line of subtitle.");
	} else {
		videoSource.pause();
		if (radioMarks.checked) {
			selectSource.options[index].setAttribute("data-start", Math.floor(videoSource.currentTime * 100));
			buildOptionText();
			radioMarke.click();
		} else if (radioMarke.checked) {
			selectSource.options[index].setAttribute("data-end", Math.floor(videoSource.currentTime * 100));
			buildOptionText();
			radioMarks.click();
			moveNextOption();
		}
		if (!videoSource.ended) {
			videoSource.play();
		}
	}
}

function buildOptionText() {
	var selectSource = document.getElementById("select-source");
	var optionSource = selectSource.options[selectSource.selectedIndex];
	optionSource.text = optionSource.value;
	if (optionSource.hasAttribute("data-end")) {
		optionSource.text = millisecondsToTime(optionSource.getAttribute("data-end")) + "," + optionSource.text;
	}
	if (optionSource.hasAttribute("data-start")) {
		optionSource.text = millisecondsToTime(optionSource.getAttribute("data-start")) + "," + optionSource.text;
	}
}

function moveNextOption() {
	var selectSource = document.getElementById("select-source");
	var index = selectSource.selectedIndex;
	selectSource.selectedIndex = selectSource.options.length - 1;
	selectSource.selectedIndex = index - 1;
	selectSource.selectedIndex = index + 1;
}

function exportTXT() {
	var selectSource = document.getElementById("select-source");
	var anchorExportTXT = document.getElementById("anchor-export-txt");
	anchorExportTXT.href = "data:text/plain,";
	var href = "";
	for (var i in selectSource.options) {
		var option = selectSource.options[i];
		if (option instanceof HTMLOptionElement) {
			href += "%0D" + option.text;
		}
	}
	anchorExportTXT.href += href.substring(3);
	anchorExportTXT.click();
}

function exportSRT() {
	var selectSource = document.getElementById("select-source");
	var anchorExportSRT = document.getElementById("anchor-export-srt");
	anchorExportSRT.href = "data:text/plain,";
	var href = "";
	for (var i in selectSource.options) {
		var option = selectSource.options[i];
		if (option instanceof HTMLOptionElement) {
			href += "%0D%0D" + i +
				"%0D" + millisecondsToTime(option.getAttribute("data-start")) +
				" --> " + millisecondsToTime(option.getAttribute("data-end")) +
				"%0D" + option.value;
		}
	}
	anchorExportSRT.href += href.substring(6);
	anchorExportSRT.click();
}

function exportASS() {
	var selectSource = document.getElementById("select-source");
	var anchorExportASS = document.getElementById("anchor-export-ass");
	anchorExportASS.href = "data:text/plain,[Events]%0DFormat:Layer,Start,End,Text";
	for (var i in selectSource.options) {
		var option = selectSource.options[i];
		if (option instanceof HTMLOptionElement) {
			anchorExportASS.href += "%0DDialogue:" + i +
				"," + millisecondsToTime(option.getAttribute("data-start")) +
				"," + millisecondsToTime(option.getAttribute("data-end")) +
				"," + option.value;
		}
	}
	anchorExportASS.click();
}

function showSubtitle() {
	var videoSource = document.getElementById("video-source");
	var selectSource = document.getElementById("select-source");
	for (var i in selectSource.options) {
		var option = selectSource.options[i];
		if (option instanceof HTMLOptionElement) {
			if (option.hasAttribute("data-start") && option.hasAttribute("data-end")) {
				var textSubtitle = document.getElementById("text-subtitle");
				if (-option.getAttribute("data-start") / 100 > -videoSource.currentTime && -videoSource.currentTime > -option.getAttribute("data-end") / 100) {
					textSubtitle.textContent = option.value;
					textSubtitle.style.backgroundColor = "rgb(0,0,0)";
					break;
				} else {
					textSubtitle.textContent = "";
					textSubtitle.style.backgroundColor = "rgba(0,0,0,0)";
				}
			}
		}
	}
	window.requestAnimationFrame(function () {
		showSubtitle();
	});
}