#!/bin/bash

manifest_file="manifest.json"

ln "firefox-manifest.json" "${manifest_file}"

7z a -tzip "html5-gamepad-detector.xpi.zip" "css" "js" "logo" "svg" "index.html" "${manifest_file}"

rm -R "${manifest_file}"
