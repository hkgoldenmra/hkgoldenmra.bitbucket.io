#!/bin/bash

manifest_file="manifest.json"

ln "chrome-manifest.json" "${manifest_file}"

7z a -tzip "html5-gamepad-detector.crx.zip" "css" "js" "logo" "svg" "index.html" "${manifest_file}"

rm -R "${manifest_file}"
