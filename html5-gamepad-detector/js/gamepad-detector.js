function GamepadDetector(gamepadContainer, defaultGamepadTemplate, forceGamepadTemplate){

	var userAgent = new UserAgent();
	var gamepadDrivers = new GenericGamepadDrivers();
	var gamepadInteractors = {};

	console.log("Your OS is " + userAgent.getOS());
	console.log("Your browser is " + userAgent.getBrowser());

	this.start = function(){
		// loads the connected Gamepad object into GamepadDetector object if the driver supported.
		window.addEventListener("gamepadconnected", function(event){
			var gamepad = event.gamepad;
			var id = gamepad.id + "[" + gamepad.index + "]";
			var userAgent = new UserAgent();
			var driver = gamepadDrivers.getDriver(userAgent.getOS(), userAgent.getBrowser(), gamepad.id);
			if (gamepad instanceof Gamepad && driver != null){
				gamepad = new GenericGamepad(gamepad, driver);
				var gamepadTemplate = forceGamepadTemplate;
				if (gamepadTemplate == null){
					gamepadTemplate = document.getElementById(gamepad.getStyle() + "-gamepad-template");
					if (gamepadTemplate == null){
						gamepadTemplate = defaultGamepadTemplate;
					}
				}
				gamepadTemplate = gamepadTemplate.cloneNode(true);
				gamepadTemplate.setAttribute("id", id);
				gamepadContainer.appendChild(gamepadTemplate);
				var gamepadInteractor = new GenericGamepadInteractor(gamepad, gamepadTemplate);
				gamepadInteractor.start();
				gamepadInteractors[id] = gamepadInteractor;
				console.log("Gamepad: \"" + id + "\" is connected.");
			}
		});

		// unloads the disconnected Gamepad object from GamepadDetector object if the driver supported.
		window.addEventListener("gamepaddisconnected", function(event){
			var gamepad = event.gamepad;
			var id = gamepad.id + "[" + gamepad.index + "]";
			var gamepadDrivers = new GenericGamepadDrivers();
			var userAgent = new UserAgent();
			var driver = gamepadDrivers.getDriver(userAgent.getOS(), userAgent.getBrowser(), gamepad.id);
			if (gamepad instanceof Gamepad && driver != null){
				var gamepadTemplate = document.getElementById(id);
				gamepadTemplate.parentNode.removeChild(gamepadTemplate);
				gamepadInteractors[id].stop();			
				delete gamepadInteractors[id];
				console.log("Gamepad: \"" + id + "\" is disconnected.");
			}
		});
	};

	this.stop = function(){
		window.removeEventListener("gamepadconnected", function(event){});
		window.removeEventListener("gamepaddisconnected", function(event){});
	};
}
