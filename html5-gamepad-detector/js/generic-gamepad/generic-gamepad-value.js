function GenericGamepadValue(index, references, value){

	var defaultDPadReferences = ["du", "dd", "dl", "dr"];

	/**
	* private attribute
	* uses Gamepad object button index as index.
	*/
	var _index = index;

	/**
	* private attribute
	* uses template element references as references.
	*/
	var _references = references;

	/**
	* private attribute
	* uses Gamepad object button value as value.
	*/
	var _value = value.toFixed(7);
	_value = _value.substring(0, _value.length - 1);

	var _previousValue = 0;
	var _upKeepPressTime = null;
	var _downKeepPressTime = null;
	var _leftKeepPressTime = null;
	var _rightKeepPressTime = null;
	var _upPressedTimes = [];
	var _downPressedTimes = [];
	var _leftPressedTimes = [];
	var _rightPressedTimes = [];
	var _upReleasedTimes = [];
	var _downReleasedTimes = [];
	var _leftReleasedTimes = [];
	var _rightReleasedTimes = [];

	var _updateValue = function(value){
		_value = value.toFixed(7);
		_value = _value.substring(0, _value.length - 1);
	};
	
	this.updateValue = function(value){
		_updateValue(value);
	};

	/**
	* public function
	* gets the index of this GenericGamepadValue object.
	*/
	this.getIndex = function(){
		return _index;
	};

	/**
	* public function
	* gets the references of this GenericGamepadValue object.
	*/
	this.getReferences = function(){
		return _references;
	};

	/**
	* public function
	* gets the value of this GenericGamepadValue object.
	*/
	this.getValue = function(){
		return _value;
	};

	this.getUpKeepPressTime = function(){
		return _upKeepPressTime;
	};

	this.getUpPressedTimes = function(){
		return _upPressedTimes;
	};

	this.getUpReleasedTimes = function(){
		return _upReleasedTimes;
	};

	this.getDownKeepPressTime = function(){
		return _downKeepPressTime;
	};

	this.getDownPressedTimes = function(){
		return _downPressedTimes;
	};

	this.getDownReleasedTimes = function(){
		return _downReleasedTimes;
	};

	this.getLeftKeepPressTime = function(){
		return _leftKeepPressTime;
	};

	this.getLeftPressedTimes = function(){
		return _leftPressedTimes;
	};

	this.getLeftReleasedTimes = function(){
		return _leftReleasedTimes;
	};

	this.getRightKeepPressTime = function(){
		return _rightKeepPressTime;
	};

	this.getRightPressedTimes = function(){
		return _rightPressedTimes;
	};

	this.getRightReleasedTimes = function(){
		return _rightReleasedTimes;
	};

	this.updateStatus = function(){
		// up
		if (_value == "-1.000000"){
			if (_value == _previousValue){
				// keep press
				_upKeepPressTime = new Date();
			} else {
				// pressed
				_upPressedTimes.push(new Date());
			}
		} else if (_value == "0.142857"){
			if (_value == _previousValue){
				// keep press
				_downKeepPressTime = new Date();
			} else {
				// pressed
				_downPressedTimes.push(new Date());
			}
		} else if (_value == "0.714285"){
			if (_value == _previousValue){
				// keep press
				_leftKeepPressTime = new Date();
			} else {
				// pressed
				_leftPressedTimes.push(new Date());
			}
		} else if (_value == "-0.428571"){
			if (_value == _previousValue){
				// keep press
				_rightKeepPressTime = new Date();
			} else {
				// pressed
				_rightPressedTimes.push(new Date());
			}
		} else if (_value == "1.000000"){
			if (_value == _previousValue){
				// keep press
				_upKeepPressTime = new Date();
				_leftKeepPressTime = new Date();
			} else {
				// pressed
				_upPressedTimes.push(new Date());
				_leftPressedTimes.push(new Date());
			}
		} else if (_value == "-0.714285"){
			if (_value == _previousValue){
				// keep press
				_upKeepPressTime = new Date();
				_rightKeepPressTime = new Date();
			} else {
				// pressed
				_upPressedTimes.push(new Date());
				_rightPressedTimes.push(new Date());
			}
		} else if (_value == "0.428571"){
			if (_value == _previousValue){
				// keep press
				_downKeepPressTime = new Date();
				_leftKeepPressTime = new Date();
			} else {
				// pressed
				_downPressedTimes.push(new Date());
				_leftPressedTimes.push(new Date());
			}
		} else if (_value == "-0.142857"){
			if (_value == _previousValue){
				// keep press
				_downKeepPressTime = new Date();
				_rightKeepPressTime = new Date();
			} else {
				// pressed
				_downPressedTimes.push(new Date());
				_rightPressedTimes.push(new Date());
			}
		} else {
			if (_value == _previousValue){
				// keep release
			} else {
				if (_previousValue == "-1.000000"){
					_upReleasedTimes.push(new Date());
				} else if (_previousValue == "0.142857"){
					_downReleasedTimes.push(new Date());
				} else if (_previousValue == "0.714285"){
					_leftReleasedTimes.push(new Date());
				} else if (_previousValue == "-0.428571"){
					_rightReleasedTimes.push(new Date());
				} else if (_previousValue == "1.000000"){
					_upReleasedTimes.push(new Date());
					_leftReleasedTimes.push(new Date());
				} else if (_previousValue == "-0.714285"){
					_upReleasedTimes.push(new Date());
					_rightReleasedTimes.push(new Date());
				} else if (_previousValue == "0.428571"){
					_downReleasedTimes.push(new Date());
					_leftReleasedTimes.push(new Date());
				} else if (_previousValue == "-0.142857"){
					_downReleasedTimes.push(new Date());
					_rightReleasedTimes.push(new Date());
				}
			}
		}
		_previousValue = _value;
	};
}
