function GenericGamepad(gamepad, mapping){

	/**
	* private attribute
	* stores the mapping.
	*/
	var _mapping = mapping;

	/**
	* private attribute
	* uses Gamepad object id as driver.
	*/
	var _driver = "" + gamepad.id;

	/**
	* private attribute
	* uses Gamepad object index as index.
	*/
	var _index = "" + gamepad.index;

	/**
	* private attribute
	* combines driver and index as id.
	*/
	var _id = _driver + "[" + _index + "]";

	/**
	* private attribute
	* uses Gamepad object style as style.
	*/
	var _style = null;

	/**
	* private attribute
	* default empty GenericGamepadButton objects.
	*/
	var _buttons = {};

	/**
	* private attribute
	* default empty GenericGamepadZAnlog objects.
	*/
	var _zanalogs = {};

	/**
	* private attribute
	* default empty GenericGamepadAnalog objects.
	*/
	var _analogs = {};

	/**
	* private attribute
	* default empty GenericGamepadDPad objects.
	*/
	var _dpads = {};

	/**
	* private attribute
	* default empty GenericGamepadZButton objects.
	*/
	var _zbuttons = {};

	/**
	* private attribute
	* default empty GenericGamepadValue objects.
	*/
	var _values = {};
	
	// sets the attributes if the driver supported.
	var _updateGamepad = function(gamepad){
		if (mapping != null){
			var gamepadDrivers = new GenericGamepadDrivers();
			_style = mapping.style;
			for (var i in mapping.buttons){
				var button = mapping.buttons[i];
				if (button != null){
					if (button.type == gamepadDrivers.TYPE_BUTTON()){
						if (i in _buttons){
							_buttons[i].updateValue(gamepad.buttons[i].value);
						} else {
							_buttons[i] = new GenericGamepadButton(i, button.key, gamepad.buttons[i].value);
						}
					} else if (button.type == gamepadDrivers.TYPE_ZANALOG()){
						if (i in _zanalogs){
							_zanalogs[i].updateValue(gamepad.buttons[i].value);
						} else {
							_zanalogs[i] = new GenericGamepadZAnalog(i, button.key, button.dimension, gamepad.buttons[i].value);
						}
					}
				}
			}
			for (var i in mapping.axes){
				var axis = mapping.axes[i];
				if (axis != null){
					if (axis.type == gamepadDrivers.TYPE_ANALOG()){
						if (i in _analogs){
							_analogs[i].updateValue(gamepad.axes[i]);
						} else {
							_analogs[i] = new GenericGamepadAnalog(i, axis.key, axis.dimension, gamepad.axes[i]);
						}
					} else if (axis.type == gamepadDrivers.TYPE_DPAD()){
						if (i in _dpads){
							_dpads[i].updateValue(gamepad.axes[i]);
						} else {
							_dpads[i] = new GenericGamepadDPad(i, axis.key, gamepad.axes[i]);
						}
					} else if (axis.type == gamepadDrivers.TYPE_ZBUTTON()){
						if (i in _zbuttons){
							_zbuttons[i].updateValue(gamepad.axes[i]);
						} else {
							_zbuttons[i] = new GenericGamepadZButton(i, axis.key, gamepad.axes[i]);
						}
					} else if (axis.type == gamepadDrivers.TYPE_VALUE()){
						if (i in _values){
							_values[i].updateValue(gamepad.axes[i]);
						} else {
							_values[i] = new GenericGamepadValue(i, axis.key, gamepad.axes[i]);
						}
					}
				}
			}
		}
	}
	
	this.updateGamepad = function(gamepad){
		_updateGamepad(gamepad);
	}

	_updateGamepad(gamepad);

	/**
	* public function
	* gets the mapping of this GenericGamepad object.
	*/
	this.getMapping = function(){
		return _mapping;
	};

	/**
	* public function
	* gets the driver of this GenericGamepad object.
	*/
	this.getDriver = function(){
		return _driver;
	};

	/**
	* public function
	* gets the index of this GenericGamepad object.
	*/
	this.getIndex = function(){
		return _index;
	};

	/**
	* public function
	* gets the id of this GenericGamepad object.
	*/
	this.getId = function(){
		return _id;
	};

	/**
	* public function
	* gets the style of this GenericGamepad object.
	*/
	this.getStyle = function(){
		return _style;
	};

	/**
	* public function
	* gets the GenericGamepadButton objects of this GenericGamepad object.
	*/
	this.getButtons = function(){
		return _buttons;
	};

	/**
	* public function
	* gets the GenericGamepadZAnalog objects of this GenericGamepad object.
	*/
	this.getZAnalogs = function(){
		return _zanalogs;
	};

	/**
	* public function
	* gets the GenericGamepadAnalog objects of this GenericGamepad object.
	*/
	this.getAnalogs = function(){
		return _analogs;
	};

	/**
	* public function
	* gets the GenericGamepadDPad objects of this GenericGamepad object.
	*/
	this.getDPads = function(){
		return _dpads;
	};

	/**
	* public function
	* gets the GenericGamepadZButton objects of this GenericGamepad object.
	*/
	this.getZButtons = function(){
		return _zbuttons;
	};

	/**
	* public function
	* gets the GenericGamepadValue objects of this GenericGamepad object.
	*/
	this.getValues = function(){
		return _values;
	};
}
