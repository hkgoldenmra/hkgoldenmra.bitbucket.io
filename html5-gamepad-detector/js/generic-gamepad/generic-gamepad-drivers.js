function GenericGamepadDrivers(){

	var _OS_ANDROID = "Android";
	var _OS_LINUX = "Linux";
	var _OS_MAC = "Mac OS X";
	var _OS_WINDOWS = "Windows";

	var _BROWSER_CHROME = "Chrome";
	var _BROWSER_FIREFOX = "Firefox";

	var _STYLE_DEFAULT = "default";
	var _STYLE_PS3 = "ps3";
	var _STYLE_PS4 = "ps4";
	var _STYLE_XBOXB360 = "xbox360";
	var _STYLE_ARCADE = "arcade";

	var _TYPE_BUTTON = "button";
	var _TYPE_ZANALOG = "zanalog";
	var _TYPE_ANALOG = "analog";
	var _TYPE_DPAD = "dpad";
	var _TYPE_ZBUTTON = "zbutton";
	var _TYPE_VALUE = "value";

	var _BUTTON_CROSS = "cr";
	var _BUTTON_CIRCLE = "ci";
	var _BUTTON_SQUARE = "sq";
	var _BUTTON_TRIANGLE = "tr";
	var _BUTTON_LEFT_1 = "l1";
	var _BUTTON_RIGHT_1 = "r1";
	var _BUTTON_LEFT_2 = "l2";
	var _BUTTON_RIGHT_2 = "r2";
	var _BUTTON_SELECT = "se";
	var _BUTTON_START = "st";
	var _BUTTON_LEFT_3 = "l3";
	var _BUTTON_RIGHT_3 = "r3";
	var _BUTTON_DPAD_UP = "du";
	var _BUTTON_DPAD_DOWN = "dd";
	var _BUTTON_DPAD_LEFT = "dl";
	var _BUTTON_DPAD_RIGHT = "dr";
	var _BUTTON_HOME = "ho";
	var _BUTTON_TOUCH = "to";

	var _BUTTON_LEFT = _BUTTON_LEFT_1;
	var _BUTTON_RIGHT = _BUTTON_RIGHT_1;

	var BUTTON_SMS_1 = _BUTTON_SQUARE;
	var BUTTON_SMS_2 = _BUTTON_CROSS;

	var BUTTON_NES_A = _BUTTON_CROSS;
	var BUTTON_NES_B = _BUTTON_SQUARE;

	var BUTTON_GENE_A = _BUTTON_SQUARE;
	var BUTTON_GENE_B = _BUTTON_CROSS;
	var BUTTON_GENE_C = _BUTTON_CIRCLE;

	var _BUTTON_SFC_A = _BUTTON_CIRCLE;
	var _BUTTON_SFC_B = _BUTTON_CROSS;
	var _BUTTON_SFC_X = _BUTTON_TRIANGLE;
	var _BUTTON_SFC_Y = _BUTTON_SQUARE;
	var _BUTTON_SFC_L = _BUTTON_LEFT_1;
	var _BUTTON_SFC_R = _BUTTON_RIGHT_1;
	var _BUTTON_SFC_LEFT = _BUTTON_LEFT_1;
	var _BUTTON_SFC_RIGHT = _BUTTON_RIGHT_1;

	var _BUTTON_XBOX_A = _BUTTON_CROSS;
	var _BUTTON_XBOX_B = _BUTTON_CIRCLE;
	var _BUTTON_XBOX_X = _BUTTON_SQUARE;
	var _BUTTON_XBOX_Y = _BUTTON_TRIANGLE;
	var _BUTTON_LEFT_BUMPER = _BUTTON_LEFT_1;
	var _BUTTON_RIGHT_BUMPER = _BUTTON_RIGHT_1;
	var _BUTTON_LEFT_SHOULDER = _BUTTON_LEFT_1;
	var _BUTTON_RIGHT_SHOULDER = _BUTTON_RIGHT_1;
	var _BUTTON_LEFT_TRIGGER = _BUTTON_LEFT_2;
	var _BUTTON_RIGHT_TRIGGER = _BUTTON_RIGHT_2;
	var _BUTTON_LEFT_STICK = _BUTTON_LEFT_3;
	var _BUTTON_RIGHT_STICK = _BUTTON_RIGHT_3;
	var _BUTTON_BACK = _BUTTON_SELECT;

	var _BUTTON_SHARE = _BUTTON_SELECT;
	var _BUTTON_OPTIONS = _BUTTON_START;

	var _AXIS_NULL = null;
	var _AXIS_X = "x";
	var _AXIS_Y = "y";

	this.TYPE_BUTTON = function(){
		return _TYPE_BUTTON;
	};

	this.TYPE_ZANALOG = function(){
		return _TYPE_ZANALOG;
	};

	this.TYPE_ANALOG = function(){
		return _TYPE_ANALOG;
	};

	this.TYPE_DPAD = function(){
		return _TYPE_DPAD;
	};

	this.TYPE_ZBUTTON = function(){
		return _TYPE_ZBUTTON;
	};

	this.TYPE_VALUE = function(){
		return _TYPE_VALUE;
	};

	this.AXIS_NULL = function(){
		return _AXIS_NULL;
	};

	this.AXIS_X = function(){
		return _AXIS_X;
	};

	this.AXIS_Y = function(){
		return _AXIS_Y;
	};

	var _addDriver = function(os, browser, id, mapping){
		if (!(os in _drivers)){
			_drivers[os] = {};
		}
		if (!(browser in _drivers[os])){
			_drivers[os][browser] = {};
		}
		_drivers[os][browser][id] = mapping;
	};

	var _drivers = {};

	this.getDriver = function(os, browser, id){
		if (os in _drivers && browser in _drivers[os] && id in _drivers[os][browser]){
			return _drivers[os][browser][id];
		} else {
			return null;
		}
	};

	/**
	* @OS Android 6.0.1
	* @Browser Chrome 73.0.3683.90
	* @Gamepad PS3
	* @Remark
	* major supported
	*/
	_addDriver(_OS_ANDROID, _BROWSER_CHROME, "Sony PLAYSTATION(R)3 Controller", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Android 6.0.1
	* @Browser Chrome 73.0.3683.90
	* @Gamepad PS4
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_ANDROID, _BROWSER_CHROME, "Sony Interactive Entertainment Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Android 6.0.1
	* @Browser Chrome 73.0.3683.90
	* @Gamepad PS4
	* @Remark
	* wireless
	* major supported
	*/
	_addDriver(_OS_ANDROID, _BROWSER_CHROME, "Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Android 6.0.1
	* @Browser Chrome 73.0.3683.90
	* @Gamepad PS4
	* @Remark
	* major buttons not support
	* 2 gamepad will be connected
	*/
	_addDriver(_OS_ANDROID, _BROWSER_CHROME, "Twin USB Joystick", {
		"style": _STYLE_PS3,
		"buttons": {
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Android 6.0.1
	* @Browser Chrome 73.0.3683.90
	* @Gamepad XBox360
	* @Remark
	* major supported
	* select button will send a "back" signal
	* home button will send a "home" signal
	*/
	_addDriver(_OS_ANDROID, _BROWSER_CHROME, "Microsoft X-Box 360 pad", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Android 6.0.1
	* @Browser Firefox 66.0.2
	* @Gamepad PS3
	* @Remark
	* some buttons not support
	* driver duplicated to "android-firefox-xbox360-0", so rejected but reserved this file
	* highly not recommend
	*/
	/*
	_addDriver(_OS_ANDROID, _BROWSER_FIREFOX, "android", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});
	*/

	/**
	* @OS Android 6.0.1
	* @Browser Firefox 66.0.2
	* @Gamepad PS4
	* @Remark
	* wired
	* some buttons not support
	* driver duplicated to "android-firefox-xbox360-0", so rejected but reserved this file
	* highly not recommend
	*/
	/*
	_addDriver(_OS_ANDROID, _BROWSER_FIREFOX, "android", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});
	*/

	/**
	* @OS Android 6.0.1
	* @Browser Firefox 66.0.2
	* @Gamepad PS4
	* @Remark
	* wireless
	* some buttons not support
	* driver duplicated to "android-firefox-xbox360-0", so rejected but reserved this file
	* highly not recommend
	*/
	/*
	_addDriver(_OS_ANDROID, _BROWSER_FIREFOX, "android", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});
	*/

	/**
	* @OS Android 6.0.1
	* @Browser Firefox 66.0.2
	* @Gamepad XBox360
	* @Remark
	* major supported
	* select button will send a "back" signal
	* home button will send a "home" signal
	*/
	_addDriver(_OS_ANDROID, _BROWSER_FIREFOX, "android", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad PS4
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Sony Interactive Entertainment Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad PS4
	* @Remark
	* wireless
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_ZANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"7": {"type": _TYPE_ZANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 74.0.3729.169
	* @Gamepad PS4
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Sony Computer Entertainment Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 05c4)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 74.0.3729.169
	* @Gamepad PS4
	* @Remark
	* wireless
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 05c4)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_ZANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"7": {"type": _TYPE_ZANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad PS3
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Sony PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad PS3
	* @Remark
	* wireless
	* some buttons and axis not support or incorrect mapping
	* highly not recommend
	*/
	/*
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Sony PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});
	*/

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad Twin USB
	* @Remark
	* 3 gamepads will be connected, one of gamepads is unknown
	* home button not support
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "(null) Twin USB Joystick (Vendor: 0810 Product: 0001)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_RIGHT}, "dimension": _AXIS_X},
			"5": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad XBox360[0]
	* @Remark
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Microsoft Inc. Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e)", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Chromium (Chrome based) 73.0.3683.86
	* @Gamepad XBox360[1]
	* @Remark
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_CHROME, "Inno GamePad.. Inno GamePad.. (STANDARD GAMEPAD Vendor: 045e Product: 028e)", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 66.0.1
	* @Gamepad PS3
	* @Remark
	* wired and wireless are the same
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "054c-0268-Sony PLAYSTATION(R)3 Controller", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 66.0.1
	* @Gamepad PS4
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "054c-09cc-Sony Interactive Entertainment Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
//			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
//			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"7": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 67.0
	* @Gamepad PS4
	* @Remark
	* wired
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "054c-05c4-Sony Computer Entertainment Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
//			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
//			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"7": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 66.0.1
	* @Gamepad PS4
	* @Remark
	* wireless
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "054c-09cc-Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
//			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
//			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"7": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 67.0
	* @Gamepad PS4
	* @Remark
	* wireless
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "054c-05c4-Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
//			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
//			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"7": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 66.0.1
	* @Gamepad Twin USB
	* @Remark
	* 2 gamepads will be connected
	* home button not support
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "0810-0001-Twin USB Joystick", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"5": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Linux Mint 19.1
	* @Browser Firefox 66.0.1
	* @Gamepad XBox360
	* @Remark
	* major supported
	*/
	_addDriver(_OS_LINUX, _BROWSER_FIREFOX, "045e-028e-Microsoft X-Box 360 pad", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"4": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"5": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_LEFT, ">": _BUTTON_DPAD_RIGHT}, "dimension": _AXIS_X},
			"7": {"type": _TYPE_DPAD, "key": {"<": _BUTTON_DPAD_UP, ">": _BUTTON_DPAD_DOWN}, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Chrome 73.0.3683.103
	* @Gamepad PS3
	* @Remark
	* some buttons not support
	*/
	_addDriver(_OS_MAC, _BROWSER_CHROME, "PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Chrome 73.0.3683.103
	* @Gamepad PS4
	* @Remark
	* some buttons not support
	*/
	_addDriver(_OS_MAC, _BROWSER_CHROME, "Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/*
	* @OS Mac OS X 10.11
	* @Browser Chrome 73.0.3683.103
	* @Gamepad Twin USB
	* @Remark
	* 3 gamepads will be connected, one of gamepads is unknown
	* home button not support
	*/ 
	_addDriver(_OS_MAC, _BROWSER_CHROME, "Twin USB Joystick (Vendor: 0810 Product: 0001)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"9": {
				"type": _TYPE_VALUE,
				"key": {
					"1.000000": [_BUTTON_DPAD_UP, _BUTTON_DPAD_LEFT],
					"-1.000000": [_BUTTON_DPAD_UP],
					"-0.714285": [_BUTTON_DPAD_UP, _BUTTON_DPAD_RIGHT],
					"0.714285": [_BUTTON_DPAD_LEFT],
					"3.285714": [],
					"-0.428571": [_BUTTON_DPAD_RIGHT],
					"0.428571": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_LEFT],
					"0.142857": [_BUTTON_DPAD_DOWN],
					"-0.142857": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_RIGHT]
				},
				"dimension": _AXIS_NULL
			}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Chrome 73.0.3683.103
	* @Gamepad XBox360
	* @Remark
	* some buttons not support
	*/
	_addDriver(_OS_MAC, _BROWSER_CHROME, "Xbox 360 Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e)", {
		"style": "xbo360",
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Firefox 66.0.3
	* @Gamepad ps3
	* @Remark
	* major supported
	*/
	_addDriver(_OS_MAC, _BROWSER_FIREFOX, "54c-268-PLAYSTATION(R)3 Controller", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Firefox 66.0.3
	* @Gamepad ps4
	* @Remark
	* major supported
	*/
	_addDriver(_OS_MAC, _BROWSER_FIREFOX, "54c-9cc-Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"17": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Mac OS X 10.11
	* @Browser Firefox 66.0.3
	* @Gamepad twin-usb
	* @Remark
	* major supported
	*/
	_addDriver(_OS_MAC, _BROWSER_FIREFOX, "54c-9cc-Wireless Controller", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"17": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"9": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X}
		}
	});

	/**
	* @OS Windows 8.1
	* @Browser Iron (Chrome Based) 71.0.3700.0
	* @Gamepad PS4
	* @Remark
	* major supported
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_CHROME, "Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc)", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/*
	* @OS Windows 8.1
	* @Browser Iron (Chrome Based) 71.0.3700.0
	* @Gamepad Twin USB
	* @Remark
	* 3 gamepads will be connected, one of gamepads is unknown
	* home button not support
	*/ 
	_addDriver(_OS_WINDOWS, _BROWSER_CHROME, "Twin USB Joystick (Vendor: 0810 Product: 0001)", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"9": {
				"type": _TYPE_VALUE,
				"key": {
					"1.000000": [_BUTTON_DPAD_UP, _BUTTON_DPAD_LEFT],
					"-1.000000": [_BUTTON_DPAD_UP],
					"-0.714285": [_BUTTON_DPAD_UP, _BUTTON_DPAD_RIGHT],
					"0.714285": [_BUTTON_DPAD_LEFT],
					"3.285714": [],
					"-0.428571": [_BUTTON_DPAD_RIGHT],
					"0.428571": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_LEFT],
					"0.142857": [_BUTTON_DPAD_DOWN],
					"-0.142857": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_RIGHT]
				},
				"dimension": _AXIS_NULL
			}
		}
	});

	/**
	* @OS Windows 8.1
	* @Browser Iron (Chrome Based) 71.0.3700.0
	* @Gamepad XBox360
	* @Remark
	* major supported
	* windows 10 will send "game" signal
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_CHROME, "Xbox 360 Controller (XInput STANDARD GAMEPAD)", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL},
			"16": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Windows 8.1
	* @Browser Firefox 66.0.2
	* @Gamepad PS4
	* @Remark
	* dpad not support
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_FIREFOX, "054c-09cc-Wireless Controller", {
		"style": _STYLE_PS4,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
//			"3": {"type": _TYPE_ZBUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
//			"4": {"type": _TYPE_ZBUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Windows 8.1
	* @Browser Firefox 66.0.2
	* @Gamepad Twin USB
	* @Remark
	* 2 gamepads will be connected
	* dpad not support
	* home button not support
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_FIREFOX, "0810-0001-Twin USB Joystick", {
		"style": _STYLE_PS3,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X}
		}
	});

	/**
	* @OS Windows 8.1
	* @Browser Firefox 66.0.2
	* @Gamepad XBox360
	* @Remark
	* home button not support
	* windows 10 will send "game" signal
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_FIREFOX, "xinput", {
		"style": _STYLE_XBOXB360,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_UP, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_DOWN, "dimension": _AXIS_NULL},
			"14": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_LEFT, "dimension": _AXIS_NULL},
			"15": {"type": _TYPE_BUTTON, "key": _BUTTON_DPAD_RIGHT, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"3": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Windows 10
	* @Browser Firefox 66.0.2
	* @Gamepad Arcade
	* @Remark
	* D-Pad not support
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_FIREFOX, "1532-0401-Razer Panthera PS4", {
		"style": _STYLE_ARCADE,
		"buttons": {
			"0": _BUTTON_SQUARE,
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_TOUCH, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y}
		}
	});

	/**
	* @OS Windows 10
	* @Browser Firefox 66.0.2
	* @Gamepad Arcade
	* @Remark
	*/
	_addDriver(_OS_WINDOWS, _BROWSER_CHROME, "Razer Panthera PS4 (Vendor: 1532 Product: 0401)", {
		"style": _STYLE_ARCADE,
		"buttons": {
			"0": {"type": _TYPE_BUTTON, "key": _BUTTON_SQUARE, "dimension": _AXIS_NULL},
			"1": {"type": _TYPE_BUTTON, "key": _BUTTON_CROSS, "dimension": _AXIS_NULL},
			"2": {"type": _TYPE_BUTTON, "key": _BUTTON_CIRCLE, "dimension": _AXIS_NULL},
			"3": {"type": _TYPE_BUTTON, "key": _BUTTON_TRIANGLE, "dimension": _AXIS_NULL},
			"4": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_1, "dimension": _AXIS_NULL},
			"5": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_1, "dimension": _AXIS_NULL},
			"6": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_2, "dimension": _AXIS_NULL},
			"7": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_2, "dimension": _AXIS_NULL},
			"8": {"type": _TYPE_BUTTON, "key": _BUTTON_SELECT, "dimension": _AXIS_NULL},
			"9": {"type": _TYPE_BUTTON, "key": _BUTTON_START, "dimension": _AXIS_NULL},
			"10": {"type": _TYPE_BUTTON, "key": _BUTTON_LEFT_3, "dimension": _AXIS_NULL},
			"11": {"type": _TYPE_BUTTON, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_NULL},
			"12": {"type": _TYPE_BUTTON, "key": _BUTTON_HOME, "dimension": _AXIS_NULL},
			"13": {"type": _TYPE_BUTTON, "key": _BUTTON_TOUCH, "dimension": _AXIS_NULL}
		},
		"axes": {
			"0": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_X},
			"1": {"type": _TYPE_ANALOG, "key": _BUTTON_LEFT_3, "dimension": _AXIS_Y},
			"2": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_X},
			"5": {"type": _TYPE_ANALOG, "key": _BUTTON_RIGHT_3, "dimension": _AXIS_Y},
			"9": {
				"type": _TYPE_VALUE,
				"key": {
					"1.000000": [_BUTTON_DPAD_UP, _BUTTON_DPAD_LEFT],
					"-1.000000": [_BUTTON_DPAD_UP],
					"-0.714285": [_BUTTON_DPAD_UP, _BUTTON_DPAD_RIGHT],
					"0.714285": [_BUTTON_DPAD_LEFT],
					"3.285714": [],
					"-0.428571": [_BUTTON_DPAD_RIGHT],
					"0.428571": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_LEFT],
					"0.142857": [_BUTTON_DPAD_DOWN],
					"-0.142857": [_BUTTON_DPAD_DOWN, _BUTTON_DPAD_RIGHT]
				},
				"dimension": _AXIS_NULL
			}
		}
	});
}
