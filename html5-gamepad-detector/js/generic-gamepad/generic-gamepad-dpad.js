function GenericGamepadDPad(index, references, value){

	/**
	* private attribute
	* uses Gamepad object button index as index.
	*/
	var _index = index;

	/**
	* private attribute
	* uses template element references as references.
	*/
	var _references = references;

	/**
	* private attribute
	* uses Gamepad object button value as value.
	*/
	var _value = value;

	var _previousValue = 0;
	var _negativeKeepPressTime = null;
	var _positiveKeepPressTime = null;
	var _negativePressedTimes = [];
	var _positivePressedTimes = [];
	var _negativeReleasedTimes = [];
	var _positiveReleasedTimes = [];

	var _updateValue = function(value){
		_value = value;
	};
	
	this.updateValue = function(value){
		_updateValue(value);
	};

	/**
	* public function
	* gets the index of this GenericGamepadDPad object.
	*/
	this.getIndex = function(){
		return _index;
	};

	/**
	* public function
	* gets the references of this GenericGamepadDPad object.
	*/
	this.getReferences = function(){
		return _references;
	};

	/**
	* public function
	* gets the value of this GenericGamepadDPad object.
	*/
	this.getValue = function(){
		return _value;
	};

	/**
	* public function
	* gets the direction of the value of this GenericGamepadDPad object > 0.
	*/
	this.getDirection = function(){
		if (this.getValue() < 0){
			return "<";
		} else if (this.getValue() > 0){
			return ">";
		} else {
			return "=";
		}
	};

	this.getNegativeKeepPressTime = function(){
		return _negativeKeepPressTime;
	};

	this.getPositiveKeepPressTime = function(){
		return _positiveKeepPressTime;
	};

	this.getNegativePressedTimes = function(){
		return _negativePressedTimes;
	};

	this.getPositivePressedTimes = function(){
		return _positivePressedTimes;
	};

	this.getNegativeReleasedTimes = function(){
		return _negativeReleasedTimes;
	};

	this.getPositiveReleasedTimes = function(){
		return _positiveReleasedTimes;
	};

	this.updateStatus = function(){
		if (_value < 0){
			if (_value == _previousValue){
				// negative keep press
				_negativeKeepPressTime = new Date();
			} else {
				// negative pressed
				_negativePressedTimes.push(new Date());
			}
		} else if (_value > 0){
			if (_value == _previousValue){
				// positive keep press
				_positiveKeepPressTime = new Date();
			} else {
				// positive pressed
				_positivePressedTimes.push(new Date());
			}
		} else {
			if (_value == _previousValue){
				// keep release
			} else {
				// released
				if (_previousValue < 0){
					_negativeReleasedTimes.push(new Date());
				} else if (_previousValue > 0){
					_positiveReleasedTimes.push(new Date());
				}
			}
		}
		_previousValue = _value;
	};
}
