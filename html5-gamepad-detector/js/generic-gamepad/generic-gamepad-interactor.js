function GenericGamepadInteractor(gamepad, template){

	var gamepadDrivers = new GenericGamepadDrivers();
	var start = new Date().getTime();
	var defaultDPadReferences = ["du", "dd", "dl", "dr"];
	var canUpdate = false;
	var deadZone = 0.1;

	var _updateGamepadStatus = function(gamepad){
		var gamepads = null;
		if (!!navigator.getGamepads){
			gamepads = navigator.getGamepads();
		} else if (!!navigator.mozGetGamepads){
			gamepads = navigator.mozGetGamepads();
		} else if (!!navigator.webkitGetGamepads){
			gamepads = navigator.webkitGetGamepads();
		}
		if (gamepads != null){
			for (var i in gamepads){
				if (gamepads[i].id == gamepad.getDriver() && gamepads[i].index == gamepad.getIndex()){
					gamepad.updateGamepad(gamepads[i]);
					break;
				}
			}
		}
		return gamepad;
	};

	/**
	* private function
	* updates the gamepad simulator template recurively.
	*/
	var _updateGamepad = function(){
		if (canUpdate){
console.log(0);
			gamepad = _updateGamepadStatus(gamepad);
			if (gamepad != null){
				var history = {};
				var buttons = gamepad.getButtons();
				var zanalogs = gamepad.getZAnalogs();
				var analogs = gamepad.getAnalogs();
				var dpads = gamepad.getDPads();
				var zbuttons = gamepad.getZButtons();
				var values = gamepad.getValues();
				_updateGamepadButtons(buttons);
				_updateGamepadZAnalogs(zanalogs);
				_updateGamepadAnalogs(analogs);
				_updateGamepadDPads(dpads);
				_updateGamepadZButtons(zbuttons);
				_updateGamepadValues(values);
/*
				var text = "";
				text += _updateGamepadButtonsInfo(buttons);
				text += _updateGamepadZAnalogsInfo(zanalogs);
				text += _updateGamepadAnalogsInfo(analogs);
				text += _updateGamepadDPadsInfo(dpads);
				text += _updateGamepadZButtonsInfo(zbuttons);
				text += _updateGamepadValuesInfo(values);
				var buttonsHistory = _updateGamepadButtonsHistory(buttons);
				history[buttonsHistory[0]] = buttonsHistory[1];
				var zanalogsHistory = _updateGamepadZAnalogsHistory(zanalogs);
				history[zanalogsHistory[0]] = zanalogsHistory[1];
				var analogsHistory = _updateGamepadAnalogsHistory(analogs);
				history[analogsHistory[0]] = analogsHistory[1];
				var dpadsHistory = _updateGamepadDPadsHistory(dpads);
				history[dpadsHistory[0]] = dpadsHistory[1];
				var zbuttonsHistory = _updateGamepadZButtonsHistory(zbuttons);
				history[zbuttonsHistory[0]] = zbuttonsHistory[1];
				var valuesHistory = _updateGamepadValuesHistory(values);
				history[valuesHistory[0]] = valuesHistory[1];
				var elementInfo = _referenceToElement("info text");
				elementInfo.style.display = "table-cell";
				elementInfo.value = text.substring(1);
				var elementHistory = _referenceToElement("history");
				elementHistory.style.display = "table-cell";
				elementHistory.value = JSON.stringify(history, null, "\t");
*/
				if (!!window.requestAnimationFrame){
					window.requestAnimationFrame(function(){
						_updateGamepad();
					});
				} else if (!!window.mozRequestAnimationFrame){
					window.mozRequestAnimationFrame(function(){
						_updateGamepad();
					});
				} else if (!!window.webkitRequestAnimationFrame){
					window.webkitRequestAnimationFrame(function(){
						_updateGamepad();
					});
				} else {
					window.setTimeout(function(){
						_updateGamepad();
					}, 16);
				}
			}
		}
	};

	var _referenceToElement = function(reference){
		var elements = template.getElementsByClassName("template " + reference);
		if (elements.length > 0){
			return elements[0];
		} else {
			return null;
		}
	};

	var _updateGamepadElementInfo = function(reference, releasedTimes, keepPressTime, pressedTimes, dimension){
		var text = "";
		text += "\n";
		text += reference;
		if (dimension != null){
			text += "-";
			text += dimension;
		}
		text += ": ";
		text += ((pressedTimes.length > 0) ? pressedTimes[pressedTimes.length - 1].getTime() - start : 0) + ", ";
		text += ((keepPressTime == null) ? 0 : keepPressTime.getTime() - start) + ", ";
		text += ((releasedTimes.length > 0) ? releasedTimes[releasedTimes.length - 1].getTime() - start : 0) + ", ";
		text += pressedTimes.length + ", ";
		return text;
	};

	var _updateGamepadElementHistory = function(reference, releasedTimes, pressedTimes, dimension){
		var _releasedTimes = [];
		for (var i in releasedTimes){
			_releasedTimes[i] = releasedTimes[i] - start;
		}
		var _pressedTimes = [];
		for (var i in pressedTimes){
			_pressedTimes[i] = pressedTimes[i] - start;
		}
		var json = {
			"pressedTimes": _pressedTimes,
			"releasedTimes": _releasedTimes
		};
		if (dimension != null){
			reference += "-";
			reference += dimension;
		}
		return [reference, json];
	};

	var _updateGamepadButtonsInfo = function(buttons){
		var text = "";
		for (var i in buttons){
			var button = buttons[i];
			text += _updateGamepadButtonInfo(button);
		}
		return text;
	};

	var _updateGamepadButtonInfo = function(button){
		button.updateStatus();
		return _updateGamepadElementInfo(button.getReference(), button.getReleasedTimes(), button.getKeepPressTime(), button.getPressedTimes(), null);
	};

	var _updateGamepadButtonsHistory = function(buttons){
		var json = {};
		for (var i in buttons){
			var button = buttons[i];
			var array = _updateGamepadButtonHistory(button);
			json[array[0]] = array[1];
		}
		return ["button", json];
	};

	var _updateGamepadButtonHistory = function(button){
		button.updateStatus();
		return _updateGamepadElementHistory(button.getReference(), button.getReleasedTimes(), button.getPressedTimes(), null);
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadButton objects.
	* @param buttons the GenericGamepadButton objects
	*/
	var _updateGamepadButtons = function(buttons){
		for (var i in buttons){
			var button = buttons[i];
			_updateGamepadButton(button);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadButton object.
	* @param button the GenericGamepadButton object
	*/
	var _updateGamepadButton = function(button){
		var reference = button.getReference();
		var elements = [
			_referenceToElement(reference),
			_referenceToElement(reference + "-al")

		];
		for (var i in elements){
			var element = elements[i];
			if (element != null){
				element.setAttribute("fill-opacity", ((button.isPressed()) ? 1 : 0));
			}
		}
	};

	var _updateGamepadZAnalogsInfo = function(zanalogs){
		var text = "";
		for (var i in zanalogs){
			var zanalog = zanalogs[i];
			text += _updateGamepadZAnalogInfo(zanalog);
		}
		return text;
	};

	var _updateGamepadZAnalogInfo = function(zanalog){
		zanalog.updateStatus();
		return _updateGamepadElementInfo(zanalog.getReference(), zanalog.getReleasedTimes(), zanalog.getKeepPressTime(), zanalog.getPressedTimes(), zanalog.getDimension());
	};

	var _updateGamepadZAnalogsHistory = function(zanalogs){

		var json = {};
		for (var i in zanalogs){
			var zanalog = zanalogs[i];
			var array = _updateGamepadZAnalogHistory(zanalog);
			json[array[0]] = array[1];
		}
		return ["zanalogs", json];
	};

	var _updateGamepadZAnalogHistory = function(zanalog){
		zanalog.updateStatus();
		return _updateGamepadElementHistory(zanalog.getReference(), zanalog.getReleasedTimes(), zanalog.getPressedTimes(), null);
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadZAnalog object.
	* @param zanalogs the GenericGamepadZAnalog object
	*/
	var _updateGamepadZAnalogs = function(zanalogs){
		for (var i in zanalogs){
			var zanalog = zanalogs[i];
			_updateGamepadZAnalog(zanalog);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadAnalog object.
	* @param zanalog the GenericGamepadAnalog object
	*/
	var _updateGamepadZAnalog = function (zanalog){
		var reference = zanalog.getReference();
		var dimension = zanalog.getDimension();
		var value = zanalog.getValue();
		var element = _referenceToElement(reference);
		var shadowElement = _referenceToElement(reference + "-shadow");
		var baseElement = _referenceToElement(reference + "-base");
		_updateGamepadStick(dimension, value, element, shadowElement, baseElement);
	};

	var _updateGamepadAnalogsInfo = function(analogs){
		var text = "";
		for (var i in analogs){
			var analog = analogs[i];
			text += _updateGamepadAnalogInfo(analog);
		}
		return text;
	};

	var _updateGamepadAnalogInfo = function(analog){
		analog.updateStatus();
		return _updateGamepadElementInfo(analog.getReference(), analog.getReleasedTimes(), analog.getKeepPressTime(), analog.getPressedTimes(), analog.getDimension());
	};

	var _updateGamepadAnalogsHistory = function(analogs){
		var json = {};
		for (var i in analogs){
			var analog = analogs[i];
			var array = _updateGamepadAnalogHistory(analog);
			json[array[0]] = array[1];
		}
		return ["analogs", json];
	};

	var _updateGamepadAnalogHistory = function(analog){
		analog.updateStatus();
		return _updateGamepadElementHistory(analog.getReference(), analog.getReleasedTimes(), analog.getPressedTimes(), null);
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadAnalog objects.
	* @param analogs the GenericGamepadAnalog objects
	*/
	var _updateGamepadAnalogs = function(analogs){
		for (var i in analogs){
			var analog = analogs[i];
			_updateGamepadAnalog(analog);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadAnalog object.
	* @param analog the GenericGamepadAnalog object
	*/
	var _updateGamepadAnalog = function(analog){
		var reference = analog.getReference();
		var dimension = analog.getDimension();
		var value = analog.getValue();
		var element = _referenceToElement(reference);
		var moveElement = _referenceToElement(reference + "-move");
		var shadowElement = _referenceToElement(reference + "-shadow");
		var baseElement = _referenceToElement(reference + "-base");
		_updateGamepadStick(dimension, value, element, moveElement, shadowElement, baseElement);
		if (dimension == gamepadDrivers.AXIS_X()){
			var elementDl = _referenceToElement(reference + "-dl");
			var elementDr = _referenceToElement(reference + "-dr");
			_updateGamepadAnalogButton(value, elementDl, elementDr);
			var elementWh = _referenceToElement(reference + "-wh");
			_updateGamepadRotate(value, elementWh);
		} else if (dimension == gamepadDrivers.AXIS_Y()){
			var elementDu = _referenceToElement(reference + "-du");
			var elementDd = _referenceToElement(reference + "-dd");
			_updateGamepadAnalogButton(value, elementDu, elementDd);
		}
	};

	/**
	* private function
	* updates the stick of gamepad simulator template element from GenericGamepadAnalog object.
	* @param dimension the GenericGamepadAnalog axis
	* @param value the GenericGamepadAnalog value
	* @param element the main element of SVGElement object
	* @param moveElement the move element of SVGElement object
	* @param shadowElement the shadow element of SVGElement object
	* @param baseElement the base element of SVGElement object
	*/
	var _updateGamepadStick = function(dimension, value, element, moveElement, shadowElement, baseElement){
		if (element != null && shadowElement != null && baseElement != null){
			var r = baseElement.getAttribute("r")
			if (element.transform.baseVal.length == 0){
				element.setAttribute("transform", "translate(0,0)");
			}
			var x = element.transform.baseVal[0].matrix.e;
			var y = element.transform.baseVal[0].matrix.f;
			if (dimension == gamepadDrivers.AXIS_X()){
				x = r * value;
			} else if (dimension == gamepadDrivers.AXIS_Y()){
				y = r * value;
			}
			element.setAttribute("transform", "translate(" + x + "," + y + ")");
			shadowElement.setAttribute("transform", element.getAttribute("transform"));
			if (moveElement != null){
				moveElement.setAttribute("transform", element.getAttribute("transform"));
				x /= r;
				y /= r;
				if (x < -deadZone || x > deadZone || y < -deadZone || y > deadZone){
					moveElement.setAttribute("fill-opacity", 1);
				} else {
					moveElement.setAttribute("fill-opacity", 0);
				}
			}
		}
	};

	/**
	* private function
	* updates the button of gamepad simulator template element from GenericGamepadAnalog object.
	* @param value the GenericGamepadAnalog value
	* @param negativeElement the SVGElement object represent negative value
	* @param positiveElement the SVGElement object represent position value
	*/
	var _updateGamepadAnalogButton = function(value, negativeElement, positiveElement){
		if (negativeElement != null && positiveElement != null){
			negativeElement.setAttribute("fill-opacity", 0);
			positiveElement.setAttribute("fill-opacity", 0);
			if (value < 0){
				negativeElement.setAttribute("fill-opacity", 1);
			} else if (value > 0){
				positiveElement.setAttribute("fill-opacity", 1);
			}
		}
	};

	/**
	* private function
	* updates the rotation of gamepad simulator template element from GenericGamepadAnalog object.
	* @param value the GenericGamepadAnalog value
	* @param element the SVGElement object
	*/
	var _updateGamepadRotate = function(value, element){
		if (element != null){
			var bBox = element.getBBox();
			var cx = bBox.x + bBox.width / 2;
			var cy = bBox.y + bBox.height / 2;
			element.setAttribute("transform", "rotate(" + (value * 90) + "," + cx + "," + cy + ")");
		}
	};

	var _updateGamepadDPadsInfo = function(dpads){
		var text = "";
		for (var i in dpads){
			var dpad = dpads[i];
			text += _updateGamepadDPadInfo(dpad);
		}
		return text;
	};

	var _updateGamepadDPadInfo = function(dpad){
		dpad.updateStatus();
		var references = dpad.getReferences();
		var text = "";
		text += _updateGamepadElementInfo(references["<"], dpad.getNegativeReleasedTimes(), dpad.getNegativeKeepPressTime(), dpad.getNegativePressedTimes(), null);
		text += _updateGamepadElementInfo(references[">"], dpad.getPositiveReleasedTimes(), dpad.getPositiveKeepPressTime(), dpad.getPositivePressedTimes(), null);
		return text;
	};

	var _updateGamepadDPadsHistory = function(dpads){
		var json = {};
		for (var i in dpads){
			var dpad = dpads[i];
			var array = _updateGamepadDPadHistory(dpad);
			json[array[0][0]] = array[0][1];
			json[array[1][0]] = array[1][1];
		}
		return ["dpads", json];
	};

	var _updateGamepadDPadHistory = function(dpad){
		dpad.updateStatus();
		var references = dpad.getReferences();
		var array = [];
		array.push(_updateGamepadElementHistory(references["<"], dpad.getNegativeReleasedTimes(), dpad.getNegativePressedTimes(), null));
		array.push(_updateGamepadElementHistory(references[">"], dpad.getPositiveReleasedTimes(), dpad.getPositivePressedTimes(), null));
		return array;
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadDPad objects.
	* @param dpads the GenericGamepadDPad objects
	*/
	var _updateGamepadDPads = function(dpads){
		for (var i in dpads){
			var dpad = dpads[i];
			_updateGamepadDPad(dpad);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadDPad object.
	* @param dpad the GenericGamepadDPad object
	*/
	var _updateGamepadDPad = function(dpad){
		var references = dpad.getReferences();
		var direction = dpad.getDirection();
		for (var i in references){
			var element = _referenceToElement(references[i]);
			if (element != null){
				element.setAttribute("fill-opacity", 0);
			}
		}
		if (direction != "="){
			var element = _referenceToElement(references[direction]);
			if (element != null){
				element.setAttribute("fill-opacity", 1);
			}
		}
	};

	var _updateGamepadZButtonsInfo = function(zbuttons){
		var text = "";
		for (var i in zbuttons){
			var zbutton = zbuttons[i];
			text += _updateGamepadZButtonInfo(zbutton);
		}
		return text;
	};

	var _updateGamepadZButtonInfo = function(zbutton){
		zbutton.updateStatus();
		return _updateGamepadElementInfo(zbutton.getReference(), zbutton.getReleasedTimes(), zbutton.getKeepPressTime(),zbutton.getPressedTimes(), null);
	};

	var _updateGamepadZButtonsHistory = function(zbuttons){
		var json = {};
		for (var i in zbuttons){
			var zbutton = zbuttons[i];
			var array = _updateGamepadZButtonHistory(zbutton);
			json[array[0]] = array[1];
		}
		return ["zbuttons", json];
	};

	var _updateGamepadZButtonHistory = function(zbutton){
		zbutton.updateStatus();
		return _updateGamepadElementHistory(zbutton.getReference(), zbutton.getReleasedTimes(), zbutton.getPressedTimes(), null);
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadZButton objects.
	* @param zbuttons the GenericGamepadZButton objects
	*/
	var _updateGamepadZButtons = function(zbuttons){
		for (var i in zbuttons){
			var zbutton = zbuttons[i];
			_updateGamepadZButton(zbutton);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadZButton object.
	* @param zbutton the GenericGamepadZButton object
	*/
	var _updateGamepadZButton = function(zbutton){
		var element = _referenceToElement(zbutton.getReference());
		if (element != null){
			element.setAttribute("fill-opacity", ((zbutton.isPressed()) ? 1 : 0));
		}
	};

	var _updateGamepadValuesInfo = function(values){
		var text = "";
		for (var i in values){
			var value = values[i];
			text += _updateGamepadValueInfo(value);
		}
		return text;
	};

	var _updateGamepadValueInfo = function(value){
		value.updateStatus();
		var text = "";
		text += _updateGamepadElementInfo("du", value.getUpReleasedTimes(), value.getUpKeepPressTime(), value.getUpPressedTimes(), null);
		text += _updateGamepadElementInfo("dd", value.getDownReleasedTimes(), value.getDownKeepPressTime(), value.getDownPressedTimes(), null);
		text += _updateGamepadElementInfo("dl", value.getLeftReleasedTimes(), value.getLeftKeepPressTime(), value.getLeftPressedTimes(), null);
		text += _updateGamepadElementInfo("dr", value.getRightReleasedTimes(), value.getRightKeepPressTime(), value.getRightPressedTimes(), null);
		return text;
	};

	var _updateGamepadValuesHistory = function(values){
		var json = {};
		for (var i in values){
			var value = values[i];
			var array = _updateGamepadValueHistory(value);
			json[array[0][0]] = array[0][1];
			json[array[1][0]] = array[1][1];
			json[array[2][0]] = array[2][1];
			json[array[3][0]] = array[3][1];
		}
		return ["values", json];
	};

	var _updateGamepadValueHistory = function(value){
		value.updateStatus();
		var array = [];
		array.push(_updateGamepadElementHistory("du", value.getUpReleasedTimes(), value.getUpPressedTimes(), null));
		array.push(_updateGamepadElementHistory("dd", value.getDownReleasedTimes(), value.getDownPressedTimes(), null));
		array.push(_updateGamepadElementHistory("dl", value.getLeftReleasedTimes(), value.getLeftPressedTimes(), null));
		array.push(_updateGamepadElementHistory("dr", value.getRightReleasedTimes(), value.getRightPressedTimes(), null));
		return array;
	};

	/**
	* private function
	* updates the gamepad simulator template elements from GenericGamepadValue objects.
	* @param values the GenericGamepadValue objects
	*/
	var _updateGamepadValues = function(values){
		for (var i in values){
			var value = values[i];
			_updateGamepadValue(value);
		}
	};

	/**
	* private function
	* updates the specific gamepad simulator template element from the specific GenericGamepadValue object.
	* @param value the GenericGamepadValue object
	*/
	var _updateGamepadValue = function(value){
		_resetGamepadDPads();
		var references = value.getReferences();
		if (value.getValue() in references){
			var reference = references[value.getValue()];
			for (var i in reference){
				var element = _referenceToElement(reference[i]);
				if (element != null){
					element.setAttribute("fill-opacity", 1);
				}
			}
		}
	};

	/**
	* private function
	* resets all dpad from the gamepad simulator template elements.
	*/
	var _resetGamepadDPads = function(){
		for (var i in defaultDPadReferences){
			var defaultDPadReference = defaultDPadReferences[i];
			var element = _referenceToElement(defaultDPadReference);
			if (element != null){
				element.setAttribute("fill-opacity", 0);
			}
		}
	};

	/**
	* public function
	* starts gamepad update.
	*/
	this.start = function(){
		if (!canUpdate){
			canUpdate = true;
			_updateGamepad();
		}
	};

	/**
	* public function
	* stops gamepad update.
	*/
	this.stop = function(){
		canUpdate = false;
	};
}
