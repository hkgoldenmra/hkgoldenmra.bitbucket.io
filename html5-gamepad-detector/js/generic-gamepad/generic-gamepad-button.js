function GenericGamepadButton(index, reference, value){

	/**
	* private attribute
	* uses Gamepad object button index as index.
	*/
	var _index = index;

	/**
	* private attribute
	* uses template element reference as reference.
	*/
	var _reference = reference;

	/**
	* private attribute
	* uses Gamepad object button value as value.
	*/
	var _value = value;

	var _previousValue = 0;
	var _keepPressTime = null;
	var _pressedTimes = [];
	var _releasedTimes = [];

	var _updateValue = function(value){
		_value = value;
	};

	this.updateValue = function(value){
		_updateValue(value);
	};

	/**
	* public function
	* gets the index of this GenericGamepadButton object.
	*/
	this.getIndex = function(){
		return _index;
	};

	/**
	* public function
	* gets the reference of this GenericGamepadButton object.
	*/
	this.getReference = function(){
		return _reference;
	};

	/**
	* public function
	* gets the value of this GenericGamepadButton object.
	*/
	this.getValue = function(){
		return _value;
	};

	/**
	* public function
	* gets true if the value of this GenericGamepadButton object > 0.
	*/
	this.isPressed = function(){
		return this.getValue() > 0;
	};

	this.getKeepPressTime = function(){
		return _keepPressTime;
	};

	this.getPressedTimes = function(){
		return _pressedTimes;
	};

	this.getReleasedTimes = function(){
		return _releasedTimes;
	};

	this.updateStatus = function(){
		if (_value > 0){
			if (_value == _previousValue){
				// keep press
				_keepPressTime = new Date();
			} else {
				// pressed
				_pressedTimes.push(new Date());
			}
		} else {
			if (_value == _previousValue){
				// keep release
			} else {
				// released
				_releasedTimes.push(new Date());
			}
		}
		_previousValue = _value;
	};
}
