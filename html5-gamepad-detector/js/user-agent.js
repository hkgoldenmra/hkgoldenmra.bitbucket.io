function UserAgent(){

	var userAgent = navigator.userAgent.toLowerCase();

	/**
	* private attribute
	* stores the OS type.
	*/
	var _os = null;
	if (userAgent.indexOf("win") >= 0){
		_os = "Windows";
	} else if (userAgent.indexOf("android") >= 0){
		_os = "Android";
	} else if (userAgent.indexOf("linux") >= 0){
		_os = "Linux";
	} else if (userAgent.indexOf("mac") >= 0){
		_os = "Mac OS X";
	}

	/**
	* private attribute
	* stores the browser type.
	*/
	var _browser = null;
	if (userAgent.indexOf("firefox") >= 0){
		_browser = "Firefox";
	} else if (userAgent.indexOf("chrome") >= 0){
		_browser = "Chrome";
	}

	/**
	* public function
	* gets the detected OS type.
	*/
	this.getOS = function(){
		return _os;
	};

	/**
	* public function
	* gets the detected browser type.
	*/
	this.getBrowser = function(){
		return _browser;
	};
}
