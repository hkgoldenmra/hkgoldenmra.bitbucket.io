function scaleSVG(value){
	document.getElementById("svg-scaler").value = value;
	document.getElementById("svg-scaler-displayer").textContent = value + " %";
	var svgs = document.getElementsByTagName("svg");
	for (var i in svgs){
		var svg = svgs[i];
		if (svg instanceof SVGElement){
			svg.setAttribute("width", svg.viewBox.baseVal.width * (value / 100));
			svg.setAttribute("height", svg.viewBox.baseVal.height * (value / 100));
		}
	}
}

window.addEventListener("load", function(event){
	document.getElementById("svg-scaler").addEventListener("input", function(event){
		scaleSVG(this.value);
	});
	document.getElementById("force-gamepad-template").addEventListener("input", function(event){
		window.location.href = "?type=" + this.value + "&scale=" + document.getElementById("svg-scaler").value;
	});
	var queryString = new QueryString();
	var fields = queryString.getFields();
	fields["scale"] -= 0;
	if (100 <= fields["scale"] && fields["scale"] <= 400){
		scaleSVG(fields["scale"]);
	}
	var element = document.getElementById("force-gamepad-template");
	var options = element.options;
	for (var i in options){
		if ("type" in fields && options[i].value == fields["type"]){
			element.selectedIndex = i;
			break;
		}
	}
	var value = element.value;
	var gamepadContainer = document.getElementById("gamepad-container");
	var defaultGamepadTemplate = document.getElementById("default-gamepad-template");
	var forceGamepadTemplate = document.getElementById(value + "-gamepad-template");
	var gamepadDetector = new GamepadDetector(gamepadContainer, defaultGamepadTemplate, forceGamepadTemplate);
	gamepadDetector.start();
});
