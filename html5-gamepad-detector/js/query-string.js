function QueryString(){

	var strings = window.location.href.split("?", 2);
	var _fields = {};
	if (strings.length > 1){
		var fields = strings[1].split("&");
		for (var i in fields){
			var value = fields[i].split("=", 2);
			_fields[value[0]] = ((value.length > 1) ? value[1] : null);
		}
	}

	this.getFields = function(){
		return _fields;
	};
}
