# Abstract
Just for fun.

It is the most important reason.
# Introduction
This project is using HTML5 Gamepad API which can monitor your gamepads connected to your computer in a web browser.

However, it is not a completely compatible API for all OSes, Web Browsers, Gamepad Drivers.
# How to run
## JavaScript
You must import the JavaScript files in order:

1. js/generic-gamepad/*.js
2. js/user-agent.js
3. js/query-string.js
4. js/gamepad-detector.js
5. js/initial.js
## SVG
The gamepad template uses SVG format.

You can draw and export SVG files by using Inkscape or other SVG supported softwares. (Inkscape is a Free and Open Source Software)

The GenericGamepadInteractor object affects these elements below with specific class name in a SVG:

* template sq
	* As PlayStation Square button (left main button)
* template cr
	* As PlayStation Cross button (down main button)
* template ci
	* As PlayStation Circle button (right main button)
* template tr
	* As PlayStation Triangle button (up main button)
* template l1
	* As PlayStation L1 button (left shoulder button)
* template r1
	* As PlayStation R1 button (right shoulder button)
* template l2
	* As PlayStation L2 button (left trigger button)
* template r2
	* As PlayStation R2 button (right trigger button)
* template se
	* As PlayStation Select button (left panel button)
* template st
	* As PlayStation Start button (right panel button)
* template ho
	* As PlayStation home button (center panel button)
* template l3
	* As PlayStation L3 button (left analog button)
* template l3-al
	* As PlayStation L3 Alternative button (left analog alternative button)
* template l3-move
	* As PlayStation L3 Analog (left analog moving)
* template l3-shadow
	* As PlayStation L3 Analog (left analog button movement)
* template l3-base
	* As PlayStation L3 Analog (left analog button base position)
* template l3-du
	* As PlayStation L3 up (left analog simulates D-Pad up button)
* template l3-dd
	* As PlayStation L3 down (left analog simulates D-Pad down button)
* template l3-dl
	* As PlayStation L3 left (left analog simulates D-Pad left button)
* template l3-dr
	* As PlayStation L3 right (left analog simulates D-Pad right button)
* template r3
	* As PlayStation R3 button (right analog button)
* template r3-al
	* As PlayStation R3 Alternative button (right analog alternative button)
* template r3-move
	* As PlayStation R3 Analog (right analog moving)
* template r3-shadow
	* As PlayStation R3 Analog (right analog button movement)
* template r3-base
	* As PlayStation R3 Analog (right analog button base position)
* template r3-du
	* As PlayStation R3 up (right analog simulates D-Pad up button)
* template r3-dd
	* As PlayStation R3 down (right analog simulates D-Pad down button)
* template r3-dl
	* As PlayStation R3 left (right analog simulates D-Pad left button)
* template r3-dr
	* As PlayStation R3 right (right analog simulates D-Pad right button)
* template du
	* As PlayStation up button (D-Pad up button)
* template dd
	* As PlayStation down button (D-Pad down button)
* template dl
	* As PlayStation left button (D-Pad left button)
* template dr
	* As PlayStation right button (D-Pad right button)
## HTML
Running the Javascript code
```
var gamepadContainer = document.getElementById("gamepad-container");
var defaultGamepadTemplate = document.getElementById("default-gamepad-template");
var forceGamepadTemplate = document.getElementById("force-gamepad-template");
var gamepadDetector = new GamepadDetector(gamepadContainer, defaultGamepadTemplate, forceGamepadTemplate);
gamepadDetector.start();
```
The HTMLElement objects: gamepadContainer, defaultGamepadTemplate, forceGamepadTemplate are required.
# Available Gamepad Template
* Sony Play Station 3 gamepad
* Microsoft XBox360 gamepad
* Sony Play Station 4 gamepad
* Sony Play Station Portable
* Microsoft XBox One gamepad
* SEGA Master System gamepad
* Nintendo Entertainment System gamepad
* Nintendo Game Boy Color
* Nintendo Game Boy Advance
* Nintendo 2DS XL
* Nintendo Super Famicom gamepad
* SEGA Genesis gamepad
* SEGA Dreamcast gamepad
* Nintendo N64 gamepad
* Nintendo Switch gamepad
* Nintendo GameCube gamepad
* Sony Play Station gamepad
* Steering Wheel gamepad (No official)
* Arcade 8-Button gamepad (No official)
# Tests
* Android
	* 6.0.1
		* Samsung
			* Galaxy S5
				* Chrome
					* 73.0.3683.90
						* Sony PLAYSTATION(R)3 Controller
						* Sony Interactive Entertainment Wireless Controller (ps4, wired)
						* Wireless Controller (ps4, wireless)
						* Twin USB Joystick (twin-usb)
						* Microsoft X-Box 360 pad (xbox360)
				* Firefox
					* 66.0.2
						* android (ps3)
						* android (ps4, wired)
						* android (ps4, wireless)
						* android (xbox360)
* Linux
	* Linux Mint
		* 19.1
			* Kernel
				* 4.18.0-16
					* Chromium (Chrome Based)
						* 73.0.3683.86
							* Sony PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268) (PS3, wired)
							* Sony PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268) (PS3, wireless)
							* Sony Interactive Entertainment Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc) (ps4, wired)
							* Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc) (ps4, wireless)
							* (null) Twin USB Joystick (Vendor: 0810 Product: 0001) (twin-usb)
							* Microsoft Inc. Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e) (xbox360)
							* Inno GamePad.. Inno GamePad.. (STANDARD GAMEPAD Vendor: 045e Product: 028e) (xbox360)
						* 74.0.3729.169
							* Sony Computer Entertainment Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 05c4) (ps4, wired)
							* Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 05c4) (ps4, wireless)
					* Firefox
						* 66.0.1
							* 054c-0268-Sony PLAYSTATION(R)3 Controller (PS3, wired)
							* 054c-0268-Sony PLAYSTATION(R)3 Controller (PS3, wireless)
							* 054c-09cc-Sony Interactive Entertainment Wireless Controller (ps4, wired)
							* 054c-09cc-Wireless Controller (ps4, wireless)
							* 0810-0001-Twin USB Joystick (twin-usb)
							* 045e-028e-Microsoft X-Box 360 pad (xbox360)
						* 67.0
							* 054c-05c4-Sony Computer Entertainment Wireless Controller (ps4, wired)
							* 054c-05c4-Wireless Controller (ps4, wireless)
* Mac OS X
	* 10.11
		* Chrome
			* 73.0.3683.103
				* PLAYSTATION(R)3 Controller (STANDARD GAMEPAD Vendor: 054c Product: 0268) (ps3)
				* Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc) (ps4)
				* Twin USB Joystick (Vendor: 0810 Product: 0001) (twin-usb)
				* Xbox 360 Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e) (xbox360)
		* Firefox
			* 66.0.3
				* 54c-268-PLAYSTATION(R)3 Controller (ps3)
				* 54c-9cc-Wireless Controller (ps4)
				* 810-1-Twin USB Joystick (twin-usb)
* Windows
	* 8.1
		* Iron (Chrome Based)
			* 71.0.3700.0
				* Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc) (ps4, wired)
				* Twin USB Joystick (Vendor: 0810 Product: 0001) (twin-usb)
				* Xbox 360 Controller (XInput STANDARD GAMEPAD) (xbox360)
		* Firefox
			* 66.0.2
				* 054c-09cc-Wireless Controller (ps4)
				* 0810-0001-Twin USB Joystick (twin-usb)
				* xinput (xbox360)
	* 10
		* Chrome
			* 73.0.3683.103
				* Wireless Controller (STANDARD GAMEPAD Vendor: 054c Product: 09cc) (ps4, wired; provided by zaft_officer)
				* Twin USB Joystick (Vendor: 0810 Product: 0001) (twin-usb)
				* Xbox 360 Controller (XInput STANDARD GAMEPAD) (Logitech F710; provided by zaft_officer)
				* Razer Panthera PS4 (Vendor: 1532 Product: 0401) (Razer Panthera; provided by oragejan)
		* Firefox
			* 66.0.3
				* 054c-09cc-Wireless Controller (ps4, wired; provided by zaft_officer)
				* 0810-0001-Twin USB Joystick (twin-usb)
				* xinput (Logitech F710; provided by zaft_officer)
				* 1532-0401-Razer Panthera PS4 (Razer Panthera; provided by oragejan)
## Tester Credits
* [yunholam2014 @ GitLab](https://gitlab.com/yunholam2014 "yunholam2014 @ GitLab")
* [relax_kuma @ Twitch](https://www.twitch.tv/relax_kuma "relax_kuma @ Twitch")
* [扎多軍官 (zaft_officer) @ Twitch](https://www.twitch.tv/zaft_officer "扎多軍官 (zaft_officer) @ Twitch")
* [oragejan @ Twitch](https://www.twitch.tv/oragejan "relax_kuma @ Twitch")
* [67哥 (davidmok1993) @ Twitch](https://www.twitch.tv/davidmok1993 "67哥 (davidmok1993) @ Twitch")
# Packing
## *nix
*nix systems can execute "pack-xpi.sh" and "pack-crx.sh" to pack .XPI and .CRX files for Firefox and Chrome (Based) extensions respectively.
## Windows
Windows systems can execute "pack-xpi.bat" and "pack-crx.bat" to pack .XPI and .CRX files for Firefox and Chrome (Based) extensions respectively.
# Installation
## Firefox
The .XPI Firefox extension file is located in dist/html5-gamepad-detector.xpi.

I have published this project in [HTML5 Gamepad Detector @ Firefox Add-ons](https://addons.mozilla.org/en-US/firefox/addon/html5-gamepad-detector/ "HTML5 Gamepad Detector @ Firefox Add-ons").
### Stable Edition
**!!! Stable Edition cannot install unsigned .XPI, but it allows .XPI installed in debugging mode !!!**

1. Visit "about:debugging"
2. Click "Load Temporary Add-on ..." option
3. Select the .XPI file to install

**!!! .XPI file cannot remove, otherwise the extension will be disabled !!!**
### Developer Edition
Firefox Nightly is open source web browser of Firefox Developer Edition.

1. Visit "about:config"
2. Search "xpinstall.signatures.required"
3. Set value to "false"
4. Visit "about:addons"
5. Click the "Gear" icon
6. Click "Install Add-on From file ..." option
7. Select the .XPI file to install
## Chrome (Based)
The .CRX Chrome (Based) extension file is located in dist/html5-gamepad-detector.crx.

I have published this project in [HTML5 Gamepad Detector @ Chrome Web Store](https://chrome.google.com/webstore/detail/html5-gamepad-detector/kfenghlfdhpjbnmdmbhnhhojdcofilcd "HTML5 Gamepad Detector @ Chrome Web Store").
### Stable Edition
**!!! Stable Edition cannot install unsigned .CRX !!!**
### Developer Edition
Chromium and Iron are open source web browsers of Chrome Based Developer Edition.

1. Visit "chrome://extensions"
2. Enable "Developer mode"
3. Drag the .CRX file to the page
4. Click "Add extension" to install
## Launch
After install this extension to Firefox or Chrome (Based) web browser, then hit the HTML5 Gamepad Detector icon next to the address bar and launch the page.
# Conclusion
It is not easy to handle all implementation from different OSes, Web Browsers, Gamepad Drivers.
## OS and Browsers
Most of the Gamepads show "^.+ \((STANDARD GAMEPAD )?Vendor: [0-9A-Fa-f]{4} Product: [0-9A-Fa-f]{4}\)$" pattern in Chrome.

Most of the Gamepads show "^[0-9A-Fa-f]{1,4}-[0-9A-Fa-f]{1,4}-.+$" pattern in Firefox.

The Zero prefix of Vendor number and Product number in Firefox on Mac OS X are removed.

XBox360 Gamepad shows "xinput" only in Firefox on Windows.
## Android
All Gamepad Drivers on Android in Firefox are "android", so it cannot distinguish between different Gamepad Drivers.

XBox360 Gamepad is major supported temporarily on Android.
## XBox360 Gamepad
I borrow a wired XBox360 Gamepad from my friend and I buy a new wired XBox360 Gamepad in a game shop, the only differenet is their color.

When they connected to Firefox, they call "Xbox 360 Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e)".

When they connected to Chromium on Linux, my friend's XBox360 Gamepad call "Microsoft Inc. Controller (STANDARD GAMEPAD Vendor: 045e Product: 028e)", but my new XBox360 Gamepad call "Inno GamePad.. Inno GamePad.. (STANDARD GAMEPAD Vendor: 045e Product: 028e)".

The extra information let me re-connect all my Gamepads on hand again to confirm all Gamepads' ID.
# Wired and Wireless connection
PS4 Gamepad connected on wired or wireless are different on Linux, so it let me test different OS and browsers again.
## Drivers and Mappings
Different Drivers have different mappings.

Some D-Pad buttons are "Button", some of them are "Axis", some of them are "Value".

Some Trigger buttons are "Button", some of them are "Axis".

Some Stick analogs are "Axis", some of them are "Button".

I don't know about are there still another mappings, I separate them into 6 types: Button, ZAnalog (button as axis), Analog, DPad, ZButton (axis as button), Value.
# Remark
You can visit [HTML5 Gamepad Tester](https://html5gamepad.com/ "HTML5 Gamepad Tester") to show and test the **Gamepad Information** of your connected Gamepads, such as Gamepad Drivers and Mapping.

You can visit [HTML5 Gamepad Detector](https://hkgoldenmra.bitbucket.io/html5-gamepad-detector/index.html "HTML5 Gamepad Detector") to test this project online using bitbucket.io DNS.

You can fork this repository and modify it by yourself or report the information to improve this repository.

Please report which **OS**, **OS Version**, **Web Browser**, **Gamepad Driver** you are using.

If You are using **Linux**, please also report which **Linux Distribution**, **Kernel Version** you are using.

Thank yor for the improvement.
# Update
* 2020-02-20
	* Hidden info data to prevent lag problem
	* Add window.setTimout alternative function to support browser which does not support window.requestAnimationFrame
* 2020-01-19
	* Add Simple Gamepad Template
* 2020-01-17
	* Fix XBox360 Gamepad bug
	* Add Analog moving effect
	* Add dead zone to avoid non-zero center
	* Bug Discoverted !!! Chrome based on Linux not working (Debugging)
* 2019-06-10
	* Add other PS4 drivers for Firefox and Chrome based on Linux
* 2019-05-14
	* Add screenshot
	* Add press and release times history in JSON format
* 2019-05-07
	* Update SVG L button, R button more viewable
	* Update left analog and right analog offest using -base r attribute 
	* Create a pack zip file script for publishing zip file to firefox and chrome
* 2019-05-04
	* Modify .CRX and .XPI; modify .sh and .bat to pack .CRX and .XPI
	* Update PSP SVG gamepad template
* 2019-05-03
	* Modify the Javascript uses event listener method instead of HTML event
	* Fix .CRX settings
	* Build the firefox extension (.XPI) and chrome extension (.CRX) located in dist
	* Add build script files
* 2019-04-29
	* Prevent multiple start()
* 2019-04-25
	* Add my friend's credit
* 2019-04-24
	* Add PS3 gamepad wireless drivers mapping
* 2019-04-23
	* Update all generic-gamepad-*.js
	* Add gamepad information
* 2019-04-21
	* Exploring PS4 wired and wireless connection driver are different on Linux and Android
	* Fix driver problem
	* Test press and release times
* 2019-04-20
	* Add generic-gamepad-zanalog
* 2019-04-19
	* Update generic-gamepad-interactor.js
	* Use transform and translate to move the analog simulation for all SVGElement rather than change cx, cy value which area SVGCircleElement and SVGEllipseElement attributes only
	* Refactor JS file and HTML files
	* Fix "The character encoding declaration of the HTML document was not found when prescanning the first 1024 bytes" warning 
* 2019-04-17
	* Add scale function
	* Update all SVG to version 1.1
	* Add query string parameters
	* Add Razer Panthera drivers
* 2019-04-16
	* Add default SVG gamepad templates
	* Add GenericGamepadInteractor separates GamepadDetector function
	* Add Arcade SVG gamepad templates
	* Add L3, R3 alternative button
	* Add credits
* 2019-04-15
	* Add more SVG gamepad templates
	* Add steering wheel SVG gamepad template
	* Add Left analog as steering wheel rotate simulation
	* Update gamepad detector
* 2019-04-13
	* Update index.html, default.css
* 2019-04-12
	* Remove js/drivers* and replaced by generic-gamepad-drivers.js
	* Add Mac OS X gamepad drivers
* 2019-04-10
	* Add PSP, XBox one SVG game template
	* Force change gamepad template feature
	* Add PS3 driver
* 2019-04-08
	* Add XBox360, PS4 SVG game template
	* Auto-detect gamepad style
	* Send simple information to textarea
* 2019-04-07
	* Separator all JS files by OSes, Browsers, drivers
	* Change "template an" element id to "template ho"
	* Update JS to check OS and Browser
	* Add PS3 SVG game template
* 2019-04-06
	* Fix D-Pad bugs
	* Reject and Reupdate Twin-USB driver in Firefox on Windows
* 2019-04-05
	* Modularize gamepad objects
* 2019-04-03
	* Separate HTML, JS, CSS files to individual modules
* 2019-04-02
	* Edit the popular gamepad templates
	* Change gamepad templates from element id to class to prevent non-unique id issue
* 2019-04-01
	* Move HTML5 Gamepad Detector to bitbucket.io DNS
	* Pass HTML5 Standard Verification

**Enjoy Playing Games**
