@echo off

set exe_path=
set manifest_file=manifest.json

mklink /h "%manifest_file%" "chrome-manifest.json"

"%exe_path%\7z" a -tzip "html5-gamepad-detector.crx.zip" "css" "js" "logo" "svg" "index.html" "%manifest_file%"

erase "%manifest_file%"
