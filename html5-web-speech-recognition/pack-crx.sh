#!/bin/bash

temp_dir="temp"
dist_dir="dist"

rm -R "${temp_dir}"
mkdir "${temp_dir}"

ln "manifest.json" "${temp_dir}"
ln "index.html" "${temp_dir}"
for f in "css" "js" "logo"; do
	cp -R "${f}" "${temp_dir}"
done

chromium-browser --pack-extension="`pwd`/${temp_dir}"

if ! [ -d "${dist_dir}" ]; then
	mkdir "${dist_dir}"
fi
mv "${temp_dir}.crx" "${dist_dir}/html5-web-speech-recognition.crx"
mv "${temp_dir}.pem" "${dist_dir}/html5-web-speech-recognition.pem"

rm -R "${temp_dir}"
