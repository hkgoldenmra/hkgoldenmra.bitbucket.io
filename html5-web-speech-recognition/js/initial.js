function changeFontSize(textReference, container, value){
	value -= 0;
	if (value < 10){
		value = 10;
	} else if (value > 50){
		value = 50;
	}
	var size = value + "pt";
	var width = (value * 27) + "px";
	textReference.textContent = "Size " + size;
	var transcripts = document.getElementsByClassName("transcript");
	for (var i in transcripts){
		if (transcripts[i] instanceof HTMLElement){
			transcripts[i].style.fontSize = size;
			transcripts[i].style.width = width;
		}
	}
}

function padZero(value, length){
	value = "" + parseInt(value);
	while (value.length < length){
		value = "0" + value;
	}
	return value;
}

function millisecondsToDateFormat(milliseconds){
	var hours = padZero(milliseconds / 3600000, 2);
	milliseconds %= 3600000;
	var minutes = padZero(milliseconds / 60000, 2);
	milliseconds %= 60000;
	var seconds = padZero(milliseconds / 1000, 2);
	milliseconds %= 1000;
	var decseconds = padZero(milliseconds / 10, 2);
	return hours + ":" + minutes + ":" + seconds + "." + decseconds;
}

window.addEventListener("load", function(event){
	var languageSelect = document.getElementById("language");
	var startButton = document.getElementById("start");
	var stopButton = document.getElementById("stop");
	var clearButton = document.getElementById("clear");
	var toggleButton = document.getElementById("toggle");
	var boldCheck = document.getElementById("bold");
	var italicCheck = document.getElementById("italic");
	var alignmentRadios = document.getElementsByClassName("alignment");
	var sizeText = document.getElementById("size-text");
	var sizeRange = document.getElementById("size-range");
	var transcriptContainer = document.getElementById("transcript-container");
	var originalText = document.getElementById("original-text");
	var translateToSelect = document.getElementById("translate-to");
	var addTranslateButton = document.getElementById("add-translate");
	var exportAss = document.getElementById("export-ass");
	var exportSrt = document.getElementById("export-srt");
	var downloadButton = document.getElementById("download");
	var sourceAss = document.getElementById("source-ass");
	var resetButton = document.getElementById("reset");
	var translateButton = document.getElementById("translate");
	var exportText = document.getElementById("export-text");
	var speechRecognition = new GenericSpeechRecognition(originalText);
	var queryString = new QueryString();
	var fields = queryString.getFields();
	startButton.addEventListener("click", function(event){
		if (language.value == ""){
			window.alert("Please select a Language!");
		} else {
			this.disabled = true;
			language.disabled = true;
			stopButton.disabled = false;
			var lang = languageSelect.value.split(",");
			speechRecognition.setLanguage(lang[0]);
			speechRecognition.start();
		}
	});
	stopButton.addEventListener("click", function(event){
		this.disabled = true;
		languageSelect.disabled = false;
		startButton.disabled = false;
		speechRecognition.stop();
	});
	clearButton.addEventListener("click", function(event){
		var transcripts = document.getElementsByClassName("transcript");
		for (var i in transcripts){
			if (transcripts[i] instanceof HTMLElement){
				transcripts[i].value = "";
			}
		}
	});
	toggleButton.addEventListener("click", function(event){
		document.body.className = ((document.body.className == "wb") ? "bw" : "wb");
	});
	boldCheck.addEventListener("change", function(event){
		var transcripts = document.getElementsByClassName("transcript");
		for (var i in transcripts){
			if (transcripts[i] instanceof HTMLElement){
				transcripts[i].style.fontWeight = ((this.checked) ? "bold" : "");
			}
		}
	});
	italicCheck.addEventListener("change", function(event){
		var transcripts = document.getElementsByClassName("transcript");
		for (var i in transcripts){
			if (transcripts[i] instanceof HTMLElement){
				transcripts[i].style.fontStyle = ((this.checked) ? "italic" : "");
			}
		}
	});
	for (var i in alignmentRadios){
		if (alignmentRadios[i] instanceof HTMLElement){
			alignmentRadios[i].addEventListener("change", function(event){
				var transcripts = document.getElementsByClassName("transcript");
				for (var i in transcripts){
					if (transcripts[i] instanceof HTMLElement){
						transcripts[i].style.textAlign = this.value;
					}
				}
			});
		}
	}
	sizeRange.addEventListener("input", function(event){
		changeFontSize(sizeText, originalText, this.value);
	});
	addTranslateButton.addEventListener("click", function(event){
		var div = document.createElement("div");
		var languageFrom = languageSelect.value;
		var languageTo = translateToSelect.value;
		if (languageFrom == "" || languageTo == ""){
			window.alert("Please select a Language!");
		} else {
			languageFrom = languageFrom.split(",");
			languageTo = languageTo.split(",");
			var fromTo = languageFrom[languageFrom.length - 1] + ":" + languageTo[languageTo.length - 1];
			var input = originalText.cloneNode(true);
			input.setAttribute("id", fromTo);
			input.value = "";
			div.append(input);
			var input = document.createElement("input");
			input.value = "Remove (" + fromTo + ")";
			input.type = "button";
			input.addEventListener("click", function(event){
				this.parentNode.parentNode.removeChild(this.parentNode);
			});
			div.append(input);
			transcriptContainer.appendChild(div);
		}
	});
	exportAss.addEventListener("click", function(event){
		var text = "";
		var transcripts = speechRecognition.getTranscripts();
		if (transcripts.length > 0){
			text += ";Copy this script and save it as .ass format";
			text += "\n;You can edit this script with \"Aegisub\" which is a open source, cross platform subtitle editor";
			text += "\n[Script Info]";
			text += "\nTitle: Untitled";
			text += "\nScript Type: V4.00+";
			text += "\nCollisions: Normal";
			text += "\nPlayResX: 1920";
			text += "\nPlayResY: 1080";
			text += "\nPlayDepth: 0";
			text += "\nTimer: 100.0000";
			text += "\n";
			text += "\n[Events]";
			text += "\nFormat: Layer,Start,End,Style,MarginL,MarginR,MarginV,Effect,Text";
			var previousTimestamp = 0;
			for (var i in transcripts){
				var line = "\nDialogue: 0";
				var transcript = transcripts[i];
				line += "," + millisecondsToDateFormat(previousTimestamp);
				line += "," + millisecondsToDateFormat(transcript["timestamp"]);
				line += ",,,0,0,0,";
				line += "," + transcript["transcript"];
				text += line;
				previousTimestamp = transcript["timestamp"];
			}
		}
		exportText.value = text;
	});
	exportSrt.addEventListener("click", function(event){
		var text = "";
		var transcripts = speechRecognition.getTranscripts();
		if (transcripts.length > 0){
			var previousTimestamp = 0;
			for (var i in transcripts){
				var line = "\n\n" + i;
				var transcript = transcripts[i];
				line += "\n" + millisecondsToDateFormat(previousTimestamp);
				line += " --> " + millisecondsToDateFormat(transcript["timestamp"]);
				line += "\n" + transcript["transcript"];
				text += line;
				previousTimestamp = transcript["timestamp"];
			}
		}
		exportText.value = text;
	});
	downloadButton.addEventListener("click", function(event){
		sourceAss.href = "data:text/plain," + exportText.value;
		sourceAss.click();
		sourceAss.href = "#";
	});
	resetButton.addEventListener("click", function(event){
		if (window.confirm("Are you sure reset transcript history ?")){
			speechRecognition.resetTranscripts();
		}
	});
	if ("language" in fields){
		var options = language.options;
		for (var i in options){
			if ("language" in fields && options[i].value == fields["language"]){
				language.selectedIndex = i;
				break;
			}
		}
	}
	if ("auto" in fields && fields["auto"] == 1){
		startButton.click();
	}
	if ("toggle" in fields){
		if (fields["toggle"] == 1){
			toggleButton.click();
		}
	}
	if ("bold" in fields){
		if (fields["bold"] == 1){
			boldCheck.click();
		}
	}
	if ("italic" in fields){
		if (fields["italic"] == 1){
			italicCheck.click();
		}
	}
	if ("alignment" in fields){
		for (var i in alignmentRadios){
			if (alignmentRadios[i].value == fields["alignment"]){
				alignmentRadios[i].click();
				break;
			}
		}
	}
	if ("size" in fields){
		changeFontSize(sizeText, originalText, fields["size"]);
	}
});
