function translateGoogle(originalText, textElement){
	var encode = "UTF-8";
	var method = "GET";
	var protocol = "https";
	var domain = "translate.google.com";
	var path = "/translate_a/t";
	var id = textElement.getAttribute("id").split(":");
	var url = protocol + "://" + domain + path + "?";
	url += "&client=at";
	url += "&ie=" + encode;
	url += "&oe=" + encode;
	url += "&sl=" + id[0];
	url += "&tl=" + id[1];
	url += "&text=" + originalText;
	var xmlHttpRequest = new XMLHttpRequest();
	xmlHttpRequest.open(method, url);
	xmlHttpRequest.addEventListener("readystatechange", function(){
		if (this.readyState == 4 && this.status == 200){
			var text = "";
			var json = JSON.parse(this.responseText);
			for (var j in json["sentences"]){
				if ("trans" in json["sentences"][j]){
					text += json["sentences"][j]["trans"];
				}
			}
			textElement.value = text;
		}
	});
	xmlHttpRequest.send();
}

function GenericSpeechRecognition(textElement){

	var keepRunning = false;
	var speechRecognition = null;
	var transcripts = [];
	var startDate = new Date();
	if (window.SpeechRecognition){
		speechRecognition = new window.SpeechRecognition();
	} else if (window.mozSpeechRecognition){
		speechRecognition = new window.mozSpeechRecognition();
	} else if (window.webkitSpeechRecognition){
		speechRecognition = new window.webkitSpeechRecognition();
	}
	if (speechRecognition != null){
		speechRecognition.continuous = true;
		speechRecognition.interimResults = true;
		speechRecognition.addEventListener("start", function(){
		});
		speechRecognition.addEventListener("end", function(){
			if (keepRunning){
				this.start();
			}
		});
		speechRecognition.addEventListener("result", function(){
			var resultList = event.results;
			for (var i = 0; i < resultList.length; i++){
				var result = resultList.item(i);
				for (var j = 0; j < result.length; j++){
					var alternative = result.item(j);
					var transcript = alternative.transcript;
					textElement.value = transcript;
					textElement.blur();
					textElement.focus();
					textElement.setSelectionRange(textElement.value.length, textElement.value.length);
					if (result.isFinal){
						try{
							var transcriptTexts = document.getElementsByClassName("transcript");
							for (var i in transcriptTexts){
								if (transcriptTexts[i] instanceof HTMLElement && transcriptTexts[i].getAttribute("id") != "original-text"){
									translateGoogle(textElement.value, transcriptTexts[i]);
								}
							}
						} catch (ex){
							console.log(ex);
							window.alert(ex);
						}
						var currentDate = new Date().getTime() - startDate.getTime();
						var transcriptObject = {
							"transcript": transcript,
							"timestamp": currentDate
						};
						transcripts.push(transcriptObject);
						this.stop();
						break;
					}
				}
			}
		});
	}

	this.setLanguage = function(language){
		if (speechRecognition != null){
			speechRecognition.lang = language;
		}
	};

	this.getTranscripts = function(){
		return transcripts;
	};

	this.resetTranscripts = function(){
		transcripts = [];
		startDate = new Date();
	};

	this.start = function(){
		if (speechRecognition != null && !keepRunning){
			keepRunning = true;
			speechRecognition.start();
		}
	};

	this.stop = function(){
		if (speechRecognition != null){
			keepRunning = false;
			speechRecognition.stop();
		}
	};
}
