# Abstract
Live Stream Players can add their live speech as subtitle on screen.

Hearing impaired people can know the speech from the subtitle on the screen.
# Introduction
This project is using HTML5 Web Speech Recognition API in a web browser.

However, only Chrome implemented HTML5 Web Speech Recognition API from Google Speech-To-Text Engine temporarily.

Second, using yandex translate api to translate recognized contents to target language,
but this feature is using XMLHttpRequest, according to the "Same-origin policy",
XMLHttpRequest cannot access different domain without
# How to run
You must import the JavaScript files in order:

1. js/generic-speech-recognition.js
2. js/query-string.js
3. js/initial.js

You can pass some parameters to query string to preset.

* language
	* default selected language
* auto
	* 1 as auto enable speech recognition
* toggle
	* 1 as change white text black background
* bold
	* 1 as change recognized text to bold
* italic
	* 1 as change recognized text to italic
* alignment
	* left as left alignment
	* center as left alignment
	* right as left alignment
* size
	* from 10 to 50 point size of the recognized text
# Tests
## Tester Credits
* [67哥 (davidmok1993) @ Twitch](https://www.twitch.tv/davidmok1993 "67哥 (davidmok1993) @ Twitch") (香港粵語, English, 日本語)
* [oragejan @ Twitch](https://www.twitch.tv/oragejan "oragejan @ Twitch") (香港粵語, English)
* [relax_kuma @ Twitch](https://www.twitch.tv/relax_kuma "relax_kuma @ Twitch") (香港粵語, 日本語, English)
* [chei24 @ Twitch](https://www.twitch.tv/chei24 "chei24 @ Twitch") (香港粵語, English)
* [ネコ執事 (eriol123) @ Twitch](https://www.twitch.tv/eriol123 "ネコ執事 (eriol123) @ Twitch") (香港粵語, 日本語, English)
* [女忍大橋未久 (mikuohashi) @ Twitch](https://www.twitch.tv/mikuohashi "女忍大橋未久 (mikuohashi) @ Twitch") (香港粵語)
* [遊戲王狙得 (jeffrey_te) @ Twitch](https://www.twitch.tv/jeffrey_te "遊戲王狙得 (jeffrey_te) @ Twitch") (香港粵語, English)
* [扎多軍官 (zaft_officer) @ Twitch](https://www.twitch.tv/zaft_officer "扎多軍官 (zaft_officer) @ Twitch") (香港粵語, 日本語, English)
# Packing
## *nix
*nix systems can execute "pack-crx.sh" to pack .CRX file for Chrome extensions.
## Windows
Windows systems can execute "pack-crx.bat" to pack .CRX file for Chrome extensions.
# Installation
## Chrome
The .CRX Chrome extension file is located in dist/html5-web-speech-recognition.crx.

I have published this project in [HTML5 Web Speech Recognition @ Chrome Web Store](https://chrome.google.com/webstore/detail/html5-web-speech-recognit/mlpjjoplnoinecilgblimdnjiijbigef "HTML5 Web Speech Recognition @ Chrome Web Store").
## Launch
After install this extension to Chrome web browser, then hit the HTML5 Web Speech Recognition icon next to the address bar and launch the page.

Then, allow Chrome access the microphone permission, or visit [chrome://settings/content/microphone](chrome://settings/content/microphone "chrome://settings/content/microphone") setup manually.
# Conclusion
This project depends Google Speech-To-Text Engine so user must have a high speed network to retrieve the data.
# Remark
You can visit [Web Speech API Demonstration](https://www.google.com/intl/en/chrome/demos/speech.html "Web Speech API Demonstration") to show the implementation of HTML5 Web Speech Recognition API from Google.

You can visit [HTML5 Web Speech Recognition](https://hkgoldenmra.bitbucket.io/html5-web-speech-recognition/index.html "HTML5 Web Speech Recognition") to test this project online using bitbucket.io DNS.

You can fork this repository and modify it by yourself or report the information to improve this repository.

Thank yor for the improvement.

**Enjoy the Speech**
