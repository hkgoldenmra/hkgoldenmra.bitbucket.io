function WebBluetoothAtorchSerialMonitor(json) {
	// private constant variables
	const OPERATION_TYPE = {0x01: "Report", 0x02: "Reply", 0x11: "Command"};
	const CHARGE_TYPE = {0x01: "AC", 0x02: "DC", 0x03: "USB"};
	const SERVICE_UUID = "0000ffe0-0000-1000-8000-00805f9b34fb";
	const CHARACTERISTIC_UUID = "0000ffe1-0000-1000-8000-00805f9b34fb";
	const REPEAT_IN_PERIOD = 60;
	const CHART_WIDTH = 6000;
	const CHART_HEIGHT = 3000;
	// private variables
	// name of bluetooth device
	var name = null;
	// characteristic for read and write 
	var serviceCharacteristic = null;
	// data from serial
	var data = [];
	// elements to present in user interface
	var statusHTMLDivElement = json["statusHTMLDivElement"];
	var dataHTMLDivElement = json["dataHTMLDivElement"];
	var exportLastHTMLAElement = json["exportLastHTMLAElement"];
	var exportAllHTMLAElement = json["exportAllHTMLAElement"];
	var voltageSVGPolylineElement = json["voltageSVGPolylineElement"];
	var currentSVGPolylineElement = json["currentSVGPolylineElement"];
	var wattSVGPolylineElement = json["wattSVGPolylineElement"];
	var resistanceSVGPolylineElement = json["resistanceSVGPolylineElement"];
	var usbNSVGPolylineElement = json["usbNSVGPolylineElement"];
	var usbPSVGPolylineElement = json["usbPSVGPolylineElement"];
	var temperatureCSVGPolylineElement = json["temperatureCSVGPolylineElement"];
	var temperatureFSVGPolylineElement = json["temperatureFSVGPolylineElement"];
	var totalChargeMAhSVGPolylineElement = json["totalChargeMAhSVGPolylineElement"];
	var totalChargeWhSVGPolylineElement = json["totalChargeWhSVGPolylineElement"];
	var voltageScaleSVGGroupElement = json["voltageScaleSVGGroupElement"];
	var currentScaleSVGGroupElement = json["currentScaleSVGGroupElement"];
	var wattScaleSVGGroupElement = json["wattScaleSVGGroupElement"];
	var resistanceScaleSVGGroupElement = json["resistanceScaleSVGGroupElement"];
	var mahScaleSVGGroupElement = json["mahScaleSVGGroupElement"];
	var whScaleSVGGroupElement = json["whScaleSVGGroupElement"];
	var tempCScaleSVGGroupElement = json["tempCScaleSVGGroupElement"];
	var tempFScaleSVGGroupElement = json["tempFScaleSVGGroupElement"];
	// public method
	this.connect = function() {
		if (name == null) {
			if (navigator.bluetooth) {
				navigator.bluetooth.requestDevice({
					"filters": [
						{
							"services": [
								SERVICE_UUID
							]
						}
					]
				}).then(function(device) {
					name = device.name;
					log("Connecting to device " + name);
					return device.gatt.connect();
				}).then(function(server) {
					log("Device " + name + " is connected");
					return server.getPrimaryService(SERVICE_UUID);
				}).then(function(service) {
					log("Service " + service.uuid + " is found");
					return service.getCharacteristic(CHARACTERISTIC_UUID);
				}).then(function(characteristic) {
					serviceCharacteristic = characteristic;
					log("Characteristic " + characteristic.uuid + " is found");
					return serviceCharacteristic.startNotifications().then(function() {
						log("Notification started");
						serviceCharacteristic.addEventListener("characteristicvaluechanged", characteristicvaluechangedEventHandler);
					});
				}).catch(function(exception) {
					name = null;
					log("Exception: " + exception);
				});
			} else {
				log("Browser not support Web Bluetooth API.");
			}
		}
	};
	this.disconnect = function() {
		if (name != null) {
			name = null;
			data = [];
			serviceCharacteristic.stopNotifications().then(function() {
				serviceCharacteristic.removeEventListener("characteristicvaluechanged", characteristicvaluechangedEventHandler);
				while (dataHTMLDivElement.hasChildNodes()) {
					dataHTMLDivElement.removeChild(dataHTMLDivElement.firstChild);
				}
				exportLastHTMLAElement.setAttribute("href", "##");
				exportAllHTMLAElement.setAttribute("href", "##");
				exportLastHTMLAElement.removeAttribute("download");
				exportAllHTMLAElement.removeAttribute("download");
				voltageSVGPolylineElement.removeAttribute("points");
				currentSVGPolylineElement.removeAttribute("points");
				wattSVGPolylineElement.removeAttribute("points");
				resistanceSVGPolylineElement.removeAttribute("points");
				totalChargeMAhSVGPolylineElement.removeAttribute("points");
				totalChargeWhSVGPolylineElement.removeAttribute("points");
				usbNSVGPolylineElement.removeAttribute("points");
				usbPSVGPolylineElement.removeAttribute("points");
				temperatureCSVGPolylineElement.removeAttribute("points");
				temperatureFSVGPolylineElement.removeAttribute("points");
				voltageSVGPolylineElement.removeAttribute("aria-label");
				currentSVGPolylineElement.removeAttribute("aria-label");
				wattSVGPolylineElement.removeAttribute("aria-label");
				resistanceSVGPolylineElement.removeAttribute("aria-label");
				totalChargeMAhSVGPolylineElement.removeAttribute("aria-label");
				totalChargeWhSVGPolylineElement.removeAttribute("aria-label");
				usbNSVGPolylineElement.removeAttribute("aria-label");
				usbPSVGPolylineElement.removeAttribute("aria-label");
				temperatureCSVGPolylineElement.removeAttribute("aria-label");
				temperatureFSVGPolylineElement.removeAttribute("aria-label");
			});
		}
	};
	var setChecksum = function(data) {
		let checksum = 0;
		for (let i = 2; i < data.length - 1; i++) {
			checksum = (checksum + data[i]) & 0xFF;
		}
		data[data.length - 1] = checksum ^ 0x44;
		return data;
	};
	var write = function(data) {
		data = setChecksum(data);
		let byteArray = new Uint8Array(data);
		serviceCharacteristic.writeValue(byteArray.buffer);
	};
	this.resetWh = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.resetMAh = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.resetTime = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.resetAll = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.setup = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.exit = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x32, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.nextIncrease = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x33, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	this.previousDecrease = function() {
		write([0xFF, 0x55, 0x11, 0x03, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00]);
	};
	var log = function(data) {
		data = "Status: " + data;
		statusHTMLDivElement.textContent = data;
		console.log(data);
	};
	var byteArrayToInteger = function(byteArray, msb, signed) {
		let sum = 0;
		for (let i = 0; i < byteArray.length; i++) {
			sum += byteArray[msb ? i : (byteArray.length - i - 1)] * (1 << (8 * i));
		}
		if (signed && (sum & (1 << (8 * byteArray.length - 1))) > 0) {
			sum ^= -(1 << (8 * byteArray.length));
		}
		return sum;
	};
	var formattedData = function(byteArray, index) {
		let json = {};
		json["index"] = {"field": "Index", "label": "index", "value": index};
		json["time"] = {"field": "Time", "label": "time", "value": getNow()};
		json["raw"] = {"field": "Raw Data", "label": "raw data", "value": "" + byteArray};
		json["header"] = {"field": "Header[0:1]", "label": "header", "value": "0x" + byteArray[0].toString(16).padStart(2, "0") + ",0x" + byteArray[1].toString(16).padStart(2, "0")};
		json["mt"] = {"field": "Operation Type[2]", "label": "operation type", "value": byteArrayToInteger(byteArray.slice(2, 3), false, false)};
		json["mn"] = {"field": "Operation Name", "label": "operation name", "value": OPERATION_TYPE[json["mt"]["value"]]};
		json["dt"] = {"field": "Charge Type[3]", "label": "charge type", "value": byteArrayToInteger(byteArray.slice(3, 4), false, false)};
		json["dn"] = {"field": "Charge Name", "label": "charge name", "value": CHARGE_TYPE[json["dt"]["value"]]};
		json["v"] = {"field": "Voltage[4:6]", "label": "voltage", "value": byteArrayToInteger(byteArray.slice(4, 7), false, false) / 100};
		json["a"] = {"field": "Current[7:9]", "label": "current", "value": byteArrayToInteger(byteArray.slice(7, 10), false, false) / 100};
		json["w"] = {"field": "Watt", "label": "watt", "value": (json["v"]["value"] * json["a"]["value"]).toFixed(3) - 0};
		json["r"] = {"field": "Resistance", "label": "resistance", "value": ((json["a"]["value"] == 0) ? 999999 : (json["v"]["value"] / json["a"]["value"]).toFixed(3) - 0)};
		json["tcmah"] = {"field": "Total Charge mAh[10:12]", "label": "total charge mini ampere per hour", "value": byteArrayToInteger(byteArray.slice(10, 13), false, false)};
		json["tcwh"] = {"field": "Total Charge Wh[13:16]", "label": "total charge mini watt per hour", "value": byteArrayToInteger(byteArray.slice(13, 17), false, false) / 100};
		json["usbn"] = {"field": "USB- Voltage[17:18]", "label": "u s b negative voltage", "value": byteArrayToInteger(byteArray.slice(17, 19), false, false) / 100};
		json["usbp"] = {"field": "USB+ Voltage[19:20]", "label": "u s b positive voltage", "value": byteArrayToInteger(byteArray.slice(19, 21), false, false) / 100};
		json["tc"] = {"field": "Temperature(C)[21:22]", "label": "temperature degree celsius", "value": byteArrayToInteger(byteArray.slice(21, 23), false, true)};
		json["tf"] = {"field": "Temperature(F)", "label": "temperature degree fahrenheit", "value": (json["tc"]["value"] * 9 / 5 + 32)};
		json["tch"] = {"field": "Total Charge Hours[23:24]", "label": "total charge hours", "value": byteArrayToInteger(byteArray.slice(23, 25), false, false)};
		json["tcm"] = {"field": "Total Charge Minutes[25]", "label": "total charge minutes", "value": byteArrayToInteger(byteArray.slice(25, 26), false, false)};
		json["tcs"] = {"field": "Total Charge Seconds[26]", "label": "total charge seconds", "value": byteArrayToInteger(byteArray.slice(26, 27), false, false)};
		json["tct"] = {"field": "Total Charge Time", "label": "total charge time", "value": json["tch"]["value"] + ":" + json["tcm"]["value"].toString().padStart(2, "0") + ":" + json["tcs"]["value"].toString().padStart(2, "0")};
		json["ass"] = {"field": "Auto Sleep Seconds[27]", "label": "auto sleep seconds", "value": byteArrayToInteger(byteArray.slice(27, 28), false, false)};
		json["maxv"] = {"field": "Maximum Voltage for protection[28:29]", "label": "maximum voltage for protection", "value": byteArrayToInteger(byteArray.slice(28, 30), false, false) / 100};
		json["minv"] = {"field": "Minimum Voltage for protection[30:31]", "label": "minimum voltage for protection", "value": byteArrayToInteger(byteArray.slice(30, 32), false, false) / 100};
		json["maxa"] = {"field": "Maximum Current for protection[32:33]", "label": "maximum voltage for protection", "value": byteArrayToInteger(byteArray.slice(32, 34), false, false) / 100};
		json["backlight"] = {"field": "backlight[34]", "label": "backlight", "value": byteArrayToInteger(byteArray.slice(34, 35), false, false)};
		json["checksum"] = {"field": "Checksum[35]", "label": "checksum", "value": byteArrayToInteger(byteArray.slice(35, 36), false, false)};
		return json;
	};
	var getNow = function() {
		let now = new Date();
		let year = now.getFullYear().toString().padStart(4, "0");
		let month = (now.getMonth() + 1).toString().padStart(2, "0");
		let day = now.getDate().toString().padStart(2, "0");
		let hours = now.getHours().toString().padStart(2, "0");
		let minutes = now.getMinutes().toString().padStart(2, "0");
		let seconds = now.getSeconds().toString().padStart(2, "0");
		return year + "-" + month + "-" + day + " " + hours + ":" + minutes + ":" + seconds;
	};
	var printLast = function() {
		// print last json
		var json = data[data.length - 1];
		if (!dataHTMLDivElement.hasChildNodes()) {
			for (let i in json) {
				let element = document.createElement("div");
				element.setAttribute("id", i);
				element.setAttribute("tabindex", 0);
				dataHTMLDivElement.appendChild(element);
			}
		}
		for (let i in json) {
			let element = document.getElementById(i);
			element.textContent = json[i]["field"] + ": " + json[i]["value"];
			element.setAttribute("aria-label", json[i]["label"] + ": " + json[i]["value"]);
		}
	};
	var getFieldLastValues = function(field, limit) {
		let values = data.map(function(json) {
			return json[field]["value"];
		});
		return values.slice(-limit);
	};
	var printLines = function() {
	};
	var printChart = function() {
		let voltageValues = getFieldLastValues("v", REPEAT_IN_PERIOD);
		let currentValues = getFieldLastValues("a", REPEAT_IN_PERIOD);
		let wattValues = getFieldLastValues("w", REPEAT_IN_PERIOD);
		let resistanceValues = getFieldLastValues("r", REPEAT_IN_PERIOD);
		let totalChargeMAhValues = getFieldLastValues("tcmah", REPEAT_IN_PERIOD);
		let totalChargeWhValues = getFieldLastValues("tcwh", REPEAT_IN_PERIOD);
		let usbNValues = getFieldLastValues("usbn", REPEAT_IN_PERIOD);
		let usbPValues = getFieldLastValues("usbp", REPEAT_IN_PERIOD);
		let temperatureCValues = getFieldLastValues("tc", REPEAT_IN_PERIOD);
		let temperatureFValues = getFieldLastValues("tf", REPEAT_IN_PERIOD);
		let voltageMax = Math.max(...voltageValues);
		let voltageMin = Math.min(...voltageValues);
		let currentMax = Math.max(...currentValues);
		let currentMin = Math.min(...currentValues);
		let wattMax = Math.max(...wattValues);
		let wattMin = Math.min(...wattValues);
		let resistanceMax = Math.max(...resistanceValues);
		let resistanceMin = Math.min(...resistanceValues);
		let totalChargeMAhMax = Math.max(...totalChargeMAhValues);
		let totalChargeMAhMin = Math.min(...totalChargeMAhValues);
		let totalChargeWhMax = Math.max(...totalChargeWhValues);
		let totalChargeWhMin = Math.min(...totalChargeWhValues);
		let usbNMax = Math.max(...usbNValues);
		let usbNMin = Math.min(...usbNValues);
		let usbPMax = Math.max(...usbPValues);
		let usbPMin = Math.min(...usbPValues);
		let temperatureCMax = Math.max(...temperatureCValues);
		let temperatureCMin = Math.min(...temperatureCValues);
		let temperatureFMax = Math.max(...temperatureFValues);
		let temperatureFMin = Math.min(...temperatureFValues);
		voltageMax = Math.max(voltageMax, usbPMax, usbNMax);
		voltageMin = Math.min(voltageMin, usbPMin, usbNMin);
		let voltagePoints = [];
		let currentPoints = [];
		let wattPoints = [];
		let resistancePoints = [];
		let totalChargeMAhPoints = [];
		let totalChargeWhPoints = [];
		let usbNPoints = [];
		let usbPPoints = [];
		let temperatureCPoints = [];
		let temperatureFPoints = [];
		let voltageScales = voltageScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in voltageScales) {
			let length = voltageScales.length - 1;
			voltageScales[i].textContent = (voltageMax / length * (length - i)).toFixed(3);
		}
		let currentScales = currentScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in currentScales) {
			let length = currentScales.length - 1;
			currentScales[i].textContent = (currentMax / length * (length - i)).toFixed(3);
		}
		let wattScales = wattScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in wattScales) {
			let length = wattScales.length - 1;
			wattScales[i].textContent = (wattMax / length * (length - i)).toFixed(3);
		}
		let resistanceScales = resistanceScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in resistanceScales) {
			let length = resistanceScales.length - 1;
			resistanceScales[i].textContent = (resistanceMax / length * (length - i)).toFixed(3);
		}
		let mahScales = mahScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in mahScales) {
			let length = mahScales.length - 1;
			mahScales[i].textContent = (totalChargeMAhMax / length * (length - i)).toFixed(3);
		}
		let whScales = whScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in whScales) {
			let length = whScales.length - 1;
			whScales[i].textContent = (totalChargeWhMax / length * (length - i)).toFixed(3);
		}
		let tempCScales = tempCScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in tempCScales) {
			let length = tempCScales.length - 1;
			tempCScales[i].textContent = (temperatureCMax / length * (length - i)).toFixed(3);
		}
		let tempFScales = tempFScaleSVGGroupElement.getElementsByTagName("text");
		for (let i in tempFScales) {
			let length = tempFScales.length - 1;
			tempFScales[i].textContent = (temperatureFMax / length * (length - i)).toFixed(3);
		}
		for (let i = 0; i < Math.min(REPEAT_IN_PERIOD, data.length); i++) {
			let x = i * (CHART_WIDTH / REPEAT_IN_PERIOD);
			let voltageY = (voltageValues[i] * (CHART_HEIGHT / voltageMax));
			let currentY = (currentValues[i] * (CHART_HEIGHT / currentMax));
			let wattY = (wattValues[i] * (CHART_HEIGHT / wattMax));
			let resistanceY = (resistanceValues[i] * (CHART_HEIGHT / resistanceMax));
			let totalChargeMAhY = (totalChargeMAhValues[i] * (CHART_HEIGHT / totalChargeMAhMax));
			let totalChargeWhY = (totalChargeWhValues[i] * (CHART_HEIGHT / totalChargeWhMax));
			let usbNY = (usbNValues[i] * (CHART_HEIGHT / voltageMax));
			let usbPY = (usbPValues[i] * (CHART_HEIGHT / voltageMax));
			let temperatureCY = (temperatureCValues[i] * (CHART_HEIGHT / temperatureCMax));
			let temperatureFY = (temperatureFValues[i] * (CHART_HEIGHT / temperatureFMax));
			voltagePoints.push(x + "," + voltageY);
			currentPoints.push(x + "," + currentY);
			wattPoints.push(x + "," + wattY);
			resistancePoints.push(x + "," + resistanceY);
			totalChargeMAhPoints.push(x + "," + totalChargeMAhY);
			totalChargeWhPoints.push(x + "," + totalChargeWhY);
			usbNPoints.push(x + "," + usbNY);
			usbPPoints.push(x + "," + usbPY);
			temperatureCPoints.push(x + "," + temperatureCY);
			temperatureFPoints.push(x + "," + temperatureFY);
		}
		voltageSVGPolylineElement.setAttribute("points", voltagePoints.join(" "));
		currentSVGPolylineElement.setAttribute("points", voltagePoints.join(" "));
		wattSVGPolylineElement.setAttribute("points", wattPoints.join(" "));
		resistanceSVGPolylineElement.setAttribute("points", resistancePoints.join(" "));
		totalChargeMAhSVGPolylineElement.setAttribute("points", totalChargeMAhPoints.join(" "));
		totalChargeWhSVGPolylineElement.setAttribute("points", totalChargeWhPoints.join(" "));
		usbNSVGPolylineElement.setAttribute("points", usbNPoints.join(" "));
		usbPSVGPolylineElement.setAttribute("points", usbPPoints.join(" "));
		temperatureCSVGPolylineElement.setAttribute("points", temperatureCPoints.join(" "));
		temperatureFSVGPolylineElement.setAttribute("points", temperatureFPoints.join(" "));
		voltageSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["v"]["label"] + " Line Chart: " + data[data.length - 1]["v"]["value"]);
		currentSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["a"]["label"] + " Line Chart: " + data[data.length - 1]["a"]["value"]);
		wattSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["w"]["label"] + " Line Chart: " + data[data.length - 1]["w"]["value"]);
		resistanceSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["r"]["label"] + " Line Chart: " + data[data.length - 1]["r"]["value"]);
		totalChargeMAhSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["tcmah"]["label"] + " Line Chart: " + data[data.length - 1]["tcmah"]["value"]);
		totalChargeWhSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["tcwh"]["label"] + " Line Chart: " + data[data.length - 1]["tcwh"]["value"]);
		usbNSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["usbn"]["label"] + " Line Chart: " + data[data.length - 1]["usbn"]["value"]);
		usbPSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["usbp"]["label"] + " Line Chart: " + data[data.length - 1]["usbp"]["value"]);
		temperatureCSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["tc"]["label"] + " Line Chart: " + data[data.length - 1]["tc"]["value"]);
		temperatureFSVGPolylineElement.setAttribute("aria-label", data[data.length - 1]["tf"]["label"] + " Line Chart: " + data[data.length - 1]["tf"]["value"]);
	};
	this.exportLast = function() {
		let string = "";
		let json = data[data.length - 1];
		for (let i in json) {
			string += "\n" + json[i]["field"] + ": " + json[i]["value"];
		}
		exportLastHTMLAElement.setAttribute("download", "export-last.tsv");
		exportLastHTMLAElement.setAttribute("href", "data:text/plain;UTF-8," + string.substring(1));
	};
	this.exportAll = function() {
		let string = Object.values(data[0]).map(function(object) {
			return object["field"];
		}).join("\t");
		for (let json of data) {
			string += "\n" + Object.values(json).map(function(object) {
				return object["value"];
			}).join("\t");
		}
		exportAllHTMLAElement.setAttribute("download", "export-all.tsv");
		exportAllHTMLAElement.setAttribute("href", "data:text/plain;UTF-8," + string);
	};
	var characteristicvaluechangedEventHandler = function(characteristicvaluechangedEvent) {
		let byteArray = new Uint8Array(characteristicvaluechangedEvent.target.value.buffer);
		data.push(formattedData(byteArray, data.length));
//		while (data.length > REPEAT_IN_PERIOD) {
//			data.shift();
//		}
		printLast();
		printChart();
		console.log(data[data.length - 1]);
	};
}