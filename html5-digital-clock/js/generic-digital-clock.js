function GenericDigitalClock(template){

	var canUpdate = false;
	var hide = null;
	var show = "#ffffff";
	var modbase = 10;
	var digitMapping = {
		"0": [0, 1, 2, 4, 5, 6],
		"1": [2, 5],
		"2": [0, 2, 3, 4, 6],
		"3": [0, 2, 3, 5, 6],
		"4": [1, 2, 3, 5],
		"5": [0, 1, 3, 5, 6],
		"6": [0, 1, 3, 4, 5, 6],
		"7": [0, 2, 5],
		"8": [0, 1, 2, 3, 4, 5, 6],
		"9": [0, 1, 2, 3, 5, 6]
	};

	var _setFill = function(element, value){
		if (value == null){
			element.removeAttribute("fill");
		} else {
			element.setAttribute("fill", value);
		}
	};

	var _setSeparators = function(digit){
		var elements = [
			getFirstElementByClassName("separator-3"),
			getFirstElementByClassName("separator-2"),
			getFirstElementByClassName("separator-1")
		];
		digit = (digit % modbase + modbase) % modbase;
		for (var i in elements){
			_setFill(elements[i], ((digit % 2 == 0) ? hide : show));
		}
	};

	var _setDigit = function(digit, prefix){
		var elements = [
			getFirstElementByClassName(prefix + "-0"),
			getFirstElementByClassName(prefix + "-1"),
			getFirstElementByClassName(prefix + "-2"),
			getFirstElementByClassName(prefix + "-3"),
			getFirstElementByClassName(prefix + "-4"),
			getFirstElementByClassName(prefix + "-5"),
			getFirstElementByClassName(prefix + "-6")
		];
		digit = (digit % modbase + modbase) % modbase;
		for (var i in elements){
			_setFill(elements[i], hide);
		}
		for (var i in digitMapping[digit]){
			_setFill(elements[digitMapping[digit][i]], show);
		}
	};

	var _updateTime = function(){
		if (canUpdate){
			var date = new Date();
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var seconds = date.getSeconds();
			var milliseconds = date.getMilliseconds();
			var hours2 = parseInt(hours / 10) % 10;
			var hours1 = hours % 10;
			var minutes2 = parseInt(minutes / 10) % 10;
			var minutes1 = minutes % 10;
			var seconds2 = parseInt(seconds / 10) % 10;
			var seconds1 = seconds % 10;
			var milliseconds3 = parseInt(milliseconds / 100) % 10;
			var milliseconds2 = parseInt(milliseconds / 10) % 10;
			var milliseconds1 = milliseconds % 10;
			_setDigit(hours2, "hour-2");
			_setDigit(hours1, "hour-1");
			_setDigit(minutes2, "minute-2");
			_setDigit(minutes1, "minute-1");
			_setDigit(seconds2, "second-2");
			_setDigit(seconds1, "second-1");
			_setDigit(milliseconds3, "millisecond-3");
			_setDigit(milliseconds2, "millisecond-2");
			_setDigit(milliseconds1, "millisecond-1");
			_setSeparators(seconds1);
			if (!!window.requestAnimationFrame){
				window.requestAnimationFrame(function(){
					_updateTime();
				});
			} else if (!!window.mozRequestAnimationFrame){
				window.mozRequestAnimationFrame(function(){
					_updateTime();
				});
			} else if (!!window.webkitRequestAnimationFrame){
				window.webkitRequestAnimationFrame(function(){
					_updateTime();
				});
			}
		}
	};

	var getFirstElementByClassName = function(className){
		var elements = template.getElementsByClassName(className);
		if (elements.length > 0){
			return elements[0];
		} else {
			return null;
		}
	};

	this.start = function(){
		if (!canUpdate){
			canUpdate = true;
			_updateTime();
		}
	};

	this.stop = function(){
		canUpdate = false;
	};
}

window.addEventListener("load", function(event){
	var digitalClock = new GenericDigitalClock(document.rootElement);
	digitalClock.start();
});
